
<table align="center" border="0"><tr><td align="center" width="9999">
<img src="assets/images/logos/png/logo_vert.png" align="center" width="150" alt="Project icon">

# A'Rosa-je

Application pour faire garder ses plantes.
</td></tr></table>


## Contexte

Dans le cadre de cette première MSPR, nous avons eu pour mission de réaliser une application mobile pour la société A’rosa-Je. 
L'entreprise A'rosa-je aide les particuliers à prendre soin de leurs plantes. À la suite de la pandémie, elle subit une forte hausse des demandes à laquelle elle n’a pas la capacité de répondre. Pour cela, elle a besoin de développer une option communautaire et automatique. Elle a donc fait appel à notre équipe pour réaliser une application permettant aux utilisateurs de faire garder leurs plantes avec un partage de photo et de conseils. Les fonctionnalités attendues sont donc les suivantes:

L’application doit permettre à un utilisateur de faire garder ses plantes par un autre, avec prise en photo avec son smartphone des plantes qu’il souhaite faire garder.
Cette photo permet à des botanistes, de fournir les conseils les plus appropriés à l’utilisateur qui gardera les plantes.
Afin de s’assurer que les plantes gardées sont en bon état lorsqu’un autre utilisateur s’en occupe, l’utilisateur gardant les plantes devra prendre des photos après chaque séance d’entretien.
Les photos permettent deux choses :
Les botanistes pourront prévenir d’éventuels problèmes de santé ou d’entretiens des plantes.
Les propriétaires des plantes pourront garder une tranquillité d’esprit en sachant que leurs plantes sont bien entretenues.
Les utilisateurs (propriétaires et gardiens) pourront disposer d’un moyen de se contacter afin de se coordonner pour la garde des plantes. 
L’application permet aux utilisateurs de consulter leurs profils contenant les photos des plantes qu’ils ont gardées ou fait garder.
Elle permet aux botanistes de chercher les plantes suscitant leur intérêt afin d’écrire des conseils d’entretiens pour les plantes.

Pour ce premier work-package, nous avons pour mission de :
- Affichage des plantes à faire garder.
- Prise en photo de plantes et partage.
- Ajout et visualisation de conseils concernant l’entretien d’une plante à faire garder.
- L’application doit permettre l’utilisation d’un appareil photo.
- Elle doit permettre à l’utilisateur d’enregistrer ses plantes avec leurs localisations afin de pouvoir entrer en contact avec un botaniste.
- L’application pourra communiquer avec une application web afin de soumettre des données et d’en récupérer.
- La base de données doit être faite sous SQlite.
