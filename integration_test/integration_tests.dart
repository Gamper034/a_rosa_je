import 'package:a_rosa_je/pages/login/login_page.dart';
import 'package:a_rosa_je/pages/register/cgu.dart';
import 'package:a_rosa_je/pages/register/confirm_sign_up.dart';
import 'package:a_rosa_je/pages/register/register_page.dart';
import 'package:a_rosa_je/pages/search/search_page.dart';
import 'package:a_rosa_je/pages/splash_screen.dart';
import 'package:a_rosa_je/widgets/button.dart';
import 'package:a_rosa_je/widgets/text_field.dart';
import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:integration_test/integration_test.dart';

void main() {
  IntegrationTestWidgetsFlutterBinding.ensureInitialized();

  group('Login Test', () {
    testWidgets('Enter email and password and tap login button',
        (tester) async {
      // Load the login page.
      await tester.pumpWidget(
        MaterialApp(
          debugShowCheckedModeBanner: false,
          // home: SplashScreen(),
          initialRoute: '/loading',
          routes: {
            '/loading': (context) => SplashScreen(),
            '/login': (context) => LoginPage(),
            '/cgu': (context) => const CguPage(),
            '/confirmSignUp': (context) => const ConfirmSignUp(),
            '/signup': (context) => RegisterPage(),
            '/home': (context) => SearchPage(
                  selectedPlantTypeList: const [],
                  selectedVille: "",
                ),
          },
          supportedLocales: const [
            Locale('fr', ''), // Français
            // Autres locales...
          ],
          localizationsDelegates: const [
            GlobalMaterialLocalizations.delegate,
            GlobalWidgetsLocalizations.delegate,
            GlobalCupertinoLocalizations.delegate,
          ],
        ),
      );

      int maxTries = 1000;
      int currentTry = 0;

      const storage = FlutterSecureStorage();
      await storage.delete(key: 'jwt');

      do {
        await tester.pump();
        currentTry++;
      } while (
          find.byType(LoginPage).evaluate().isEmpty && currentTry <= maxTries);

      await tester.pumpAndSettle();

      // Find the email and password fields.
      final emailField = find.descendant(
        of: find.byKey(const Key('emailFieldParent')),
        matching: find.byType(CustomTextField),
      );
      final passwordField = find.descendant(
        of: find.byKey(const Key('passwordFieldParent')),
        matching: find.byType(CustomTextField),
      );

      // Enter email and password.
      await tester.enterText(emailField, 'admin@admin.fr');
      await tester.enterText(passwordField, 'admin');

      // Find the login button and tap on it.
      final loginButton = find.descendant(
        of: find.byKey(const Key('loginButtonParent')),
        matching: find.byType(CustomButton),
      );
      await tester.tap(loginButton);

      // Trigger a frame.
      await tester.pumpAndSettle();

      // Verify that the home page is displayed.
      expect(find.byType(SearchPage), findsOneWidget);
    });

    testWidgets('Disconnect', (tester) async {
      // Load the login page.
      await tester.pumpWidget(
        MaterialApp(
          debugShowCheckedModeBanner: false,
          // home: SplashScreen(),
          initialRoute: '/loading',
          routes: {
            '/loading': (context) => SplashScreen(),
            '/login': (context) => LoginPage(),
            '/cgu': (context) => const CguPage(),
            '/confirmSignUp': (context) => const ConfirmSignUp(),
            '/signup': (context) => RegisterPage(),
            '/home': (context) => SearchPage(
                  selectedPlantTypeList: const [],
                  selectedVille: "",
                ),
          },
          supportedLocales: const [
            Locale('fr', ''), // Français
            // Autres locales...
          ],
          localizationsDelegates: const [
            GlobalMaterialLocalizations.delegate,
            GlobalWidgetsLocalizations.delegate,
            GlobalCupertinoLocalizations.delegate,
          ],
        ),
      );

      int maxTries = 1000;
      int currentTry = 0;

      const storage = FlutterSecureStorage();
      await storage.delete(key: 'jwt');

      do {
        await tester.pump();
        currentTry++;
      } while (
          find.byType(LoginPage).evaluate().isEmpty && currentTry <= maxTries);

      await tester.pumpAndSettle();

      // Find the email and password fields.
      final emailField = find.descendant(
        of: find.byKey(const Key('emailFieldParent')),
        matching: find.byType(CustomTextField),
      );
      final passwordField = find.descendant(
        of: find.byKey(const Key('passwordFieldParent')),
        matching: find.byType(CustomTextField),
      );

      // Enter email and password.
      await tester.enterText(emailField, 'admin@admin.fr');
      await tester.enterText(passwordField, 'admin');

      // Find the login button and tap on it.
      final loginButton = find.descendant(
        of: find.byKey(const Key('loginButtonParent')),
        matching: find.byType(CustomButton),
      );
      await tester.tap(loginButton);

      // Trigger a frame.
      await tester.pumpAndSettle();

      // Verify that the home page is displayed.
      final profilButton = find.byKey(const Key('profilButton'));

      await tester.tap(profilButton);

      await tester.pumpAndSettle();

      final disconnectButton = find.byKey(const Key('disconnectButton'));

      await tester.tap(disconnectButton);

      await tester.pumpAndSettle();

      // Verify that the login page is displayed.
      expect(find.byType(LoginPage), findsOneWidget);
    });
  });

  group('SearchPage tests', () {
    testWidgets('Use filters', (tester) async {
      // Load the login page.
      await tester.pumpWidget(
        MaterialApp(
          debugShowCheckedModeBanner: false,
          // home: SplashScreen(),
          initialRoute: '/loading',
          routes: {
            '/loading': (context) => SplashScreen(),
            '/login': (context) => LoginPage(),
            '/cgu': (context) => const CguPage(),
            '/confirmSignUp': (context) => const ConfirmSignUp(),
            '/signup': (context) => RegisterPage(),
            '/home': (context) => SearchPage(
                  selectedPlantTypeList: const [],
                  selectedVille: "",
                ),
          },
          supportedLocales: const [
            Locale('fr', ''), // Français
            // Autres locales...
          ],
          localizationsDelegates: const [
            GlobalMaterialLocalizations.delegate,
            GlobalWidgetsLocalizations.delegate,
            GlobalCupertinoLocalizations.delegate,
          ],
        ),
      );

      int maxTries = 1000;
      int currentTry = 0;

      const storage = FlutterSecureStorage();
      await storage.delete(key: 'jwt');

      do {
        await tester.pump();
        currentTry++;
      } while (
          find.byType(LoginPage).evaluate().isEmpty && currentTry <= maxTries);

      await tester.pumpAndSettle();

      // Find the email and password fields.
      final emailField = find.descendant(
        of: find.byKey(const Key('emailFieldParent')),
        matching: find.byType(CustomTextField),
      );
      final passwordField = find.descendant(
        of: find.byKey(const Key('passwordFieldParent')),
        matching: find.byType(CustomTextField),
      );

      // Enter email and password.
      await tester.enterText(emailField, 'admin@admin.fr');
      await tester.enterText(passwordField, 'admin');

      // Find the login button and tap on it.
      final loginButton = find.descendant(
        of: find.byKey(const Key('loginButtonParent')),
        matching: find.byType(CustomButton),
      );
      await tester.tap(loginButton);

      // Trigger a frame.
      await tester.pumpAndSettle();

      //find filter button and tap on it
      final filterButton = find.byKey(const Key('search_filter_button'));

      await tester.tap(filterButton);

      await tester.pumpAndSettle();

      //find plant type list and select 1st plant type
      final unselectedPlantTypeList =
          find.byKey(const Key('unselected_plant_types_wrap'));

      // Find the first GestureDetector that is a descendant of the unselected plant type list
      final firstGestureDetector = find
          .descendant(
            of: unselectedPlantTypeList,
            matching: find.byType(GestureDetector),
          )
          .first;

      // Tap on the first GestureDetector
      await tester.tap(firstGestureDetector);

      // Trigger a frame
      await tester.pumpAndSettle();

      //find column that contains the city field and enter the city "Montpellier"
      final localisationColumn = find.byKey(const Key('localisation_column'));

      // Find the city field (customtextfield) that is a descendant of the localisation column
      final cityField = find.descendant(
        of: localisationColumn,
        matching: find.byType(CustomTextField),
      );

      await tester.enterText(cityField, 'Montpellier');

      //Click on the validate button

      final validateButton = find.descendant(
          of: find.byKey(const Key("save_button_container")),
          matching: find.byType(CustomButton));

      await tester.tap(validateButton);

      await tester.pumpAndSettle();

      // Verify that the home page is displayed.

      expect(find.byType(SearchPage), findsOneWidget);
    });
  });

  group('Guard Details test', () {
    testWidgets('See guard details', (tester) async {
      // Load the login page.
      await tester.pumpWidget(
        MaterialApp(
          debugShowCheckedModeBanner: false,
          // home: SplashScreen(),
          initialRoute: '/loading',
          routes: {
            '/loading': (context) => SplashScreen(),
            '/login': (context) => LoginPage(),
            '/cgu': (context) => const CguPage(),
            '/confirmSignUp': (context) => const ConfirmSignUp(),
            '/signup': (context) => RegisterPage(),
            '/home': (context) => SearchPage(
                  selectedPlantTypeList: const [],
                  selectedVille: "",
                ),
          },
          supportedLocales: const [
            Locale('fr', ''), // Français
            // Autres locales...
          ],
          localizationsDelegates: const [
            GlobalMaterialLocalizations.delegate,
            GlobalWidgetsLocalizations.delegate,
            GlobalCupertinoLocalizations.delegate,
          ],
        ),
      );

      int maxTries = 1000;
      int currentTry = 0;

      const storage = FlutterSecureStorage();
      if (await storage.read(key: 'jwt') != null) {
        await storage.delete(key: 'jwt');
      }

      do {
        await tester.pump();
        currentTry++;
      } while (
          find.byType(LoginPage).evaluate().isEmpty && currentTry <= maxTries);

      await tester.pumpAndSettle();

      // Find the email and password fields.
      final emailField = find.descendant(
        of: find.byKey(const Key('emailFieldParent')),
        matching: find.byType(CustomTextField),
      );
      final passwordField = find.descendant(
        of: find.byKey(const Key('passwordFieldParent')),
        matching: find.byType(CustomTextField),
      );

      // Enter email and password.
      await tester.enterText(emailField, 'admin@admin.fr');
      await tester.enterText(passwordField, 'admin');

      // Find the login button and tap on it.
      final loginButton = find.descendant(
        of: find.byKey(const Key('loginButtonParent')),
        matching: find.byType(CustomButton),
      );
      await tester.tap(loginButton);

      // Trigger a frame.
      await tester.pumpAndSettle();

      //find guard list and select 1st guard
      final guardCard = find.byKey(const Key("guardCard0"));

      //tap on guard card
      await tester.tap(guardCard);

      await tester.pumpAndSettle();

      // Verify that the guard details page is displayed.
      expect(find.byKey(const Key("plantList")), findsOneWidget);
    });

    testWidgets('See guard advices', (tester) async {
      // Load the login page.
      await tester.pumpWidget(
        MaterialApp(
          debugShowCheckedModeBanner: false,
          // home: SplashScreen(),
          initialRoute: '/loading',
          routes: {
            '/loading': (context) => SplashScreen(),
            '/login': (context) => LoginPage(),
            '/cgu': (context) => const CguPage(),
            '/confirmSignUp': (context) => const ConfirmSignUp(),
            '/signup': (context) => RegisterPage(),
            '/home': (context) => SearchPage(
                  selectedPlantTypeList: const [],
                  selectedVille: "",
                ),
          },
          supportedLocales: const [
            Locale('fr', ''), // Français
            // Autres locales...
          ],
          localizationsDelegates: const [
            GlobalMaterialLocalizations.delegate,
            GlobalWidgetsLocalizations.delegate,
            GlobalCupertinoLocalizations.delegate,
          ],
        ),
      );

      int maxTries = 1000;
      int currentTry = 0;

      const storage = FlutterSecureStorage();
      if (await storage.read(key: 'jwt') != null) {
        await storage.delete(key: 'jwt');
      }

      do {
        await tester.pump();
        currentTry++;
      } while (
          find.byType(LoginPage).evaluate().isEmpty && currentTry <= maxTries);

      await tester.pumpAndSettle();

      // Find the email and password fields.
      final emailField = find.descendant(
        of: find.byKey(const Key('emailFieldParent')),
        matching: find.byType(CustomTextField),
      );
      final passwordField = find.descendant(
        of: find.byKey(const Key('passwordFieldParent')),
        matching: find.byType(CustomTextField),
      );

      // Enter email and password.
      await tester.enterText(emailField, 'admin@admin.fr');
      await tester.enterText(passwordField, 'admin');

      // Find the login button and tap on it.
      final loginButton = find.descendant(
        of: find.byKey(const Key('loginButtonParent')),
        matching: find.byType(CustomButton),
      );
      await tester.tap(loginButton);

      // Trigger a frame.
      await tester.pumpAndSettle();

      //find guard list and select 1st guard
      final guardCard = find.byKey(const Key("guardCard0"));

      //tap on guard card
      await tester.tap(guardCard);

      await tester.pumpAndSettle();

      final buttons = find.descendant(
        of: find.byKey(const Key('guardDetailsColumn')),
        matching: find.byType(CustomButton),
      );

      // Get the actual widgets
      final customButtonWidgetElements = tester.widgetList(buttons);
      CustomButton? advicesButton;
      // Check the text of each Text widget
      for (final customButtonWidget in customButtonWidgetElements) {
        advicesButton = customButtonWidget as CustomButton;
        if (advicesButton.label == "Conseils de botanistes") {
          break;
        }
      }

      if (advicesButton != null) {
        await tester.tap(find.byWidget(advicesButton));
      }

      await tester.pumpAndSettle();

      // Verify that the botanist advice page is displayed.
      expect(
          find.byKey(const Key("botanist_guard_advices_page")), findsOneWidget);
    });
  });
}
