import 'package:a_rosa_je/widgets/widgets.dart';
import 'package:flutter/material.dart';
import 'package:a_rosa_je/theme/theme.dart';

// ignore: must_be_immutable
class ToastConfirm extends StatelessWidget {
  final String title;
  final String content;
  final IconData icon;
  final double height;
  String theme = "light";
  final VoidCallback onPressedCancel;
  final VoidCallback onPressedConfirm;

  // Variables pour le thème
  final Color backgroundColor;
  final Color cancelColor;
  final Color cancelTextColor;
  final Color confirmColor;
  final Color confirmTextColor;
  final Color iconColor;
  final Color titleColor;
  final Color contentColor;

  // ignore: use_key_in_widget_constructors
  ToastConfirm({
    this.theme = "light",
    required this.title,
    required this.content,
    required this.icon,
    required this.height,
    required this.onPressedCancel,
    required this.onPressedConfirm,
  })  : backgroundColor = theme == 'light' ? whiteColor : primaryColor,
        cancelColor = theme == 'light' ? secondaryColor : secondaryColor,
        cancelTextColor = theme == 'light' ? textColor : whiteColor,
        confirmColor = theme == 'light' ? primaryColor : whiteColor,
        confirmTextColor = theme == 'light' ? whiteColor : primaryColor,
        iconColor = theme == 'light' ? primaryColor : whiteColor,
        titleColor = theme == 'light' ? textColor : whiteColor,
        contentColor = theme == 'light' ? textColor : whiteColor;

  @override
  Widget build(BuildContext context) {
    return Container(
      height: height,
      padding: const EdgeInsets.all(20),
      decoration: BoxDecoration(
        color: backgroundColor,
        borderRadius: BorderRadius.circular(20),
      ),
      child: Center(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Icon(
              icon,
              color: iconColor,
              size: 48,
            ),
            const SizedBox(height: 20),
            Text(
              title,
              textAlign: TextAlign.center,
              style: TextStyle(
                color: titleColor,
                fontWeight: FontWeight.w700,
                fontSize: 16,
              ),
            ),
            const SizedBox(height: 5),
            Text(
              content,
              textAlign: TextAlign.center,
              style: TextStyle(
                color: contentColor,
                fontWeight: FontWeight.w400,
                fontSize: 14,
              ),
            ),
            const SizedBox(height: 20),
            Row(
              children: [
                Expanded(
                  child: CustomButton(
                    onPressed: onPressedCancel,
                    label: "Annuler",
                    buttonColor: cancelColor,
                    textColor: cancelTextColor,
                  ),
                ),
                const SizedBox(width: 20),
                Expanded(
                  child: CustomButton(
                    onPressed: onPressedConfirm,
                    label: "Valider",
                    buttonColor: confirmColor,
                    textColor: confirmTextColor,
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
