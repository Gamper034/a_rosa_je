// ignore_for_file: use_key_in_widget_constructors

import 'package:flutter/material.dart';
import 'package:a_rosa_je/theme/theme.dart';

class CustomButton extends StatelessWidget {
  final VoidCallback onPressed;
  final String label;
  final Color buttonColor;
  final Color textColor;
  final IconData? icon;
  final bool border;

  const CustomButton({
    Key? key,
    required this.onPressed,
    required this.label,
    this.buttonColor = whiteColor,
    this.textColor = primaryColor,
    this.icon,
    this.border = false,
  });
//OutlinedButton
  @override
  Widget build(BuildContext context) {
    return OutlinedButton(
      key: key,
      onPressed: onPressed,
      style: OutlinedButton.styleFrom(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(
              10), // Ajoutez cette ligne pour un bouton carré
        ),
        backgroundColor: buttonColor,
        side: border
            ? const BorderSide(color: secondaryColor, width: 2.0)
            : BorderSide.none,
      ),
      child: SizedBox(
        width: double.infinity,
        height: 50,
        child: Center(
          child: icon != null
              ? Row(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Text(
                      label,
                      style: TextStyle(
                          color: textColor,
                          fontSize: 16,
                          fontWeight: FontWeight.w600),
                    ),
                    const SizedBox(width: 10),
                    Icon(
                      icon,
                      color: textColor,
                    ),
                  ],
                )
              : Text(
                  label,
                  style: TextStyle(color: textColor),
                ),
        ),
      ),
    );
  }
}
