import 'package:a_rosa_je/pages/conversations/conversations_list.dart';
import 'package:a_rosa_je/pages/my_guards/my_guards.dart';
import 'package:a_rosa_je/pages/profil/profil_page.dart';
import 'package:a_rosa_je/pages/publish_guard/publish_guard.dart';
import 'package:a_rosa_je/pages/search/search_page.dart';
import 'package:a_rosa_je/theme/theme.dart';
import 'package:flutter/material.dart';
import 'package:lucide_icons/lucide_icons.dart';

class BottomNavigationBarWidget extends StatelessWidget {
  final int indexSelected;
  const BottomNavigationBarWidget(this.indexSelected, {super.key});

  @override
  Widget build(BuildContext context) {
    return BottomNavigationBar(
      type: BottomNavigationBarType.fixed,
      currentIndex: indexSelected,
      selectedItemColor: primaryColor,
      selectedLabelStyle: const TextStyle(fontSize: 12),
      unselectedLabelStyle: const TextStyle(fontSize: 12),
      items: const <BottomNavigationBarItem>[
        BottomNavigationBarItem(
            icon: Icon(LucideIcons.search), label: 'Rercherche'),
        BottomNavigationBarItem(
            icon: Icon(LucideIcons.flower2), label: 'Gardes'),
        BottomNavigationBarItem(
            icon: Icon(LucideIcons.plusSquare), label: 'Publier'),
        BottomNavigationBarItem(
            icon: Icon(LucideIcons.messagesSquare), label: 'Messages'),
        BottomNavigationBarItem(icon: Icon(LucideIcons.user), label: 'Profil'),
      ],
      onTap: (index) {
        Widget page =
            SearchPage(selectedPlantTypeList: const [], selectedVille: "");
        switch (index) {
          case 1:
            // page = MyGuards.routeName;
            page = const MyGuards();
            break;
          case 2:
            page = const PublishGuard();
            break;
          case 3:
            page = const ConversationsList();
            break;
          case 4:
            page = const ProfilPage();
            break;
        }

        // Navigator.pushNamedAndRemoveUntil(context, page, (route) => false);
        Navigator.pushAndRemoveUntil(
          context,
          PageRouteBuilder(
            pageBuilder: (context, animation, secondaryAnimation) => page,
            transitionsBuilder:
                (context, animation, secondaryAnimation, child) {
              return FadeTransition(
                opacity: animation,
                child: child,
              );
            },
          ),
          (route) => false,
        );
      },
    );
  }
}
