// ignore_for_file: use_build_context_synchronously

import 'dart:convert';

import 'package:a_rosa_je/models/guard.dart';
import 'package:a_rosa_je/pages/guard_details/guard_details.dart';
import 'package:a_rosa_je/services/guard.dart';
import 'package:a_rosa_je/services/user.dart';
import 'package:a_rosa_je/widgets/status_badge_guard/status_badge.dart';
import 'package:flutter/material.dart';
import 'package:lucide_icons/lucide_icons.dart';
import 'package:a_rosa_je/theme/theme.dart';

// ignore: must_be_immutable
class GuardCard extends StatelessWidget {
  final Guard guard;
  final Function refreshParent;
  GuardCard(
      {super.key,
      required this.guard,
      required this.myGuards,
      required this.byCurrentUser,
      required this.refreshParent});

  bool myGuards;
  bool byCurrentUser;

  DateTime now = DateTime.now();

  bool get isPassed {
    DateTime endDate =
        DateTime(guard.endDate.year, guard.endDate.month, guard.endDate.day);
    DateTime today =
        DateTime(DateTime.now().year, DateTime.now().month, DateTime.now().day);

    return endDate.isBefore(today);
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.only(bottom: 20),
      child: GestureDetector(
        onTap: () async {
          var user = await UserService().getUserPreferences();

          Navigator.push(
            context,
            MaterialPageRoute(
              builder: (context) => GuardDetails(
                guard: guard,
                user: user,
              ),
            ),
          ).then((_) {
            refreshParent();
          });
        },
        child: Column(
          children: [
            Row(
              children: [
                Expanded(
                  child: SizedBox(
                    height: 200,
                    child: Stack(
                      children: [
                        SizedBox(
                          width: double.infinity,
                          child: ClipRRect(
                            borderRadius: BorderRadius.circular(20.0),
                            child: Image.memory(
                              base64Decode(
                                  guard.plants[0].image?.split(',').last ??
                                      ''), // Utilisez base64Decode ici
                              fit: BoxFit.cover,
                            ),
                          ),
                        ),
                        if (isPassed)
                          Container(
                            decoration: BoxDecoration(
                              color: Colors.grey.withOpacity(0.5),
                              borderRadius: BorderRadius.circular(20.0),
                            ),
                          ),
                        Positioned(
                          bottom: 0,
                          left: 0,
                          child: Container(
                              margin:
                                  const EdgeInsets.only(left: 10, bottom: 10),
                              child: ClipRRect(
                                borderRadius: BorderRadius.circular(7.0),
                                child: Container(
                                    color: whiteColor,
                                    height: 24,
                                    width: 94,
                                    child: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceEvenly,
                                      children: [
                                        const Icon(
                                          LucideIcons.flower2,
                                          size: 18,
                                        ),
                                        if (guard.plants.length > 1)
                                          Text(
                                            "${guard.plants.length} plantes",
                                            style:
                                                const TextStyle(fontSize: 14),
                                          ),
                                        if (guard.plants.length <= 1)
                                          Text(
                                            "${guard.plants.length} plante",
                                            style:
                                                const TextStyle(fontSize: 14),
                                          ),
                                      ],
                                    )),
                              )),
                        ),
                        myGuards ? _badgeStatusGuard() : Container(),
                      ],
                    ),
                  ),
                ),
              ],
            ),
            const SizedBox(height: 10),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Container(
                  child: byCurrentUser ? _guardianStatus() : _ownerInfo(),
                ),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: [
                    Text(
                      "${guard.startDate.day} ${GuardService.monthNames[guard.startDate.month - 1]} - ${guard.endDate.day} ${GuardService.monthNames[guard.endDate.month - 1]}",
                      style: const TextStyle(
                          color: textColor,
                          fontSize: 16,
                          fontWeight: FontWeight.w500),
                    ),
                    byCurrentUser ? Container() : _cityInfo(),
                  ],
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }

  _ownerInfo() {
    return Row(
      children: [
        CircleAvatar(
          radius: 16.0,
          backgroundImage: NetworkImage(guard.owner.avatar),
          backgroundColor: Colors.transparent,
        ),
        const SizedBox(width: 10),
        Text(
          "${guard.owner.firstname} ${guard.owner.lastname.substring(0, 1)}.",
          style: const TextStyle(color: textColor, fontSize: 14),
        ),
      ],
    );
  }

  _guardianStatus() {
    bool hasGuard = guard.guardianId != null;
    return Row(
      children: [
        Icon(
          hasGuard ? LucideIcons.check : LucideIcons.hourglass,
          color: hasGuard ? primaryColor : warningColor,
          size: 18,
        ),
        const SizedBox(width: 5),
        Text(
          hasGuard ? "Gardien validé" : "Recherche de gardien",
          style: TextStyle(
            color: hasGuard ? primaryColor : warningColor,
            fontSize: 14,
          ),
        ),
      ],
    );
  }

  _cityInfo() {
    return Text(
      "${guard.city} ${guard.zipCode}",
      style: const TextStyle(
          color: secondaryTextColor, fontSize: 12, fontWeight: FontWeight.w400),
    );
  }

  _badgeStatusGuard() {
    GuardService guardService = GuardService();
    GuardStatus status = guardService.getStatus(guard);
    return Positioned(
        top: 10, left: 10, child: StatusBadge.getBadgeStatus(status));
  }
}
