// ignore_for_file: camel_case_types, use_super_parameters, library_private_types_in_public_api

import 'package:a_rosa_je/pages/search/search_page.dart';
import 'package:a_rosa_je/services/api/data_api.dart';
import 'package:a_rosa_je/theme/theme.dart';
import 'package:a_rosa_je/widgets/widgets.dart';
import 'package:flutter/material.dart';
import 'package:lucide_icons/lucide_icons.dart';

class searchFiltersPage extends StatefulWidget {
  searchFiltersPage(
      {Key? key,
      required List<String> selectedPlantTypeList,
      required this.selectedVille})
      : selectedPlantTypeList = List.from(selectedPlantTypeList),
        super(key: key);

  final List<String> selectedPlantTypeList;
  final String selectedVille;

  @override
  _searchFiltersPageState createState() => _searchFiltersPageState();
}

class _searchFiltersPageState extends State<searchFiltersPage> {
  late Map<String, dynamic> json;
  late List<String> plantTypeList = [];
  late String selectedVille = widget.selectedVille;
  final TextEditingController controller = TextEditingController();
  late List<String>? selectedPlantTypeList = [];
  late List<String>? nonSelectedPlantTypeList = [];

  _searchFiltersPageState();

  @override
  void initState() {
    super.initState();
    setPlantTypeList();
    if (selectedVille != '') {
      controller.text = selectedVille;
    }
  }

  void setPlantTypeList() async {
    DataApi dataApi = DataApi();

    Future<Map<String, dynamic>> futureMap = dataApi.getPlantTypeList();
    // print(futureMap);

    json = await futureMap;
    setState(() {
      plantTypeList = (json['plantTypeNames'] as List<dynamic>)
          .map<String>((plantType) => plantType as String)
          .toList();
    });
    selectedPlantTypeList = widget.selectedPlantTypeList.toList();

    for (var plantType in plantTypeList) {
      if (!selectedPlantTypeList!.contains(plantType)) {
        nonSelectedPlantTypeList!.add(plantType);
      }
    }
  }

  bool isPopInProgress = false;

  List<Widget> _buildSelectedPlantTypes() {
    return [
      Container(
        alignment: Alignment.centerLeft,
        child: Text("Sélectionnés", style: ArosajeTextStyle.labelFormTextStyle),
      ),
      Wrap(
        // ignore: prefer_is_empty
        children: selectedPlantTypeList?.length == 0
            ? [
                Container(
                  margin: const EdgeInsets.symmetric(vertical: 20),
                  child: Text(
                    'Aucun type selectionné.',
                    style: TextStyle(
                      color: Colors.grey[400],
                      fontSize: ArosajeTextStyle.labelFormTextStyle.fontSize,
                      fontWeight:
                          ArosajeTextStyle.labelFormTextStyle.fontWeight,
                    ),
                  ),
                ),
              ]
            : [
                for (var plantType in selectedPlantTypeList!)
                  GestureDetector(
                    onTap: () {
                      setState(() {
                        selectedPlantTypeList!.remove(plantType);
                        nonSelectedPlantTypeList!.add(plantType);
                      });
                    },
                    child: IntrinsicWidth(
                      child: Container(
                        padding: const EdgeInsets.all(5),
                        margin: const EdgeInsets.all(5),
                        decoration: BoxDecoration(
                          color: Colors.green,
                          borderRadius: BorderRadius.circular(5),
                        ),
                        child: Row(
                          children: [
                            const Icon(
                              LucideIcons.x,
                              color: whiteColor,
                              size: 16,
                            ),
                            const SizedBox(width: 5),
                            Text(
                              plantType,
                              style: TextStyle(
                                color: whiteColor,
                                fontSize: ArosajeTextStyle
                                    .labelFormTextStyle.fontSize,
                                fontWeight: ArosajeTextStyle
                                    .labelFormTextStyle.fontWeight,
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
              ],
      ),
    ];
  }

  List<Widget> _buildUnselectedPlantTypes() {
    return [
      Container(
        alignment: Alignment.centerLeft,
        child: Text("Disponibles", style: ArosajeTextStyle.labelFormTextStyle),
      ),
      Wrap(
        key: const Key('unselected_plant_types_wrap'),
        children: [
          for (var plantType in nonSelectedPlantTypeList!)
            GestureDetector(
              onTap: () {
                setState(() {
                  nonSelectedPlantTypeList!.remove(plantType);
                  selectedPlantTypeList!.add(plantType);
                });
              },
              child: IntrinsicWidth(
                child: Container(
                  padding: const EdgeInsets.all(5),
                  margin: const EdgeInsets.all(5),
                  decoration: BoxDecoration(
                    color: Colors.grey[300],
                    borderRadius: BorderRadius.circular(5),
                  ),
                  child: Row(
                    children: [
                      const Icon(
                        LucideIcons.plus,
                        size: 16,
                      ),
                      const SizedBox(width: 5),
                      Text(plantType,
                          style: ArosajeTextStyle.labelFormTextStyle),
                    ],
                  ),
                ),
              ),
            ),
        ],
      ),
    ];
  }

  @override
  Widget build(BuildContext context) {
    if (selectedPlantTypeList!.isEmpty && nonSelectedPlantTypeList!.isEmpty) {
      return Container(
        color: whiteColor,
        child: const Center(
          child: CircularProgressIndicator(
            color: primaryColor,
          ),
        ),
      );
    } else {
      return Scaffold(
        appBar: AppBar(
          title: Align(
            alignment: Alignment.centerLeft,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text('Filtres de recherche',
                    style: ArosajeTextStyle.AppBarTextStyle),
                IconButton(
                    icon: const Icon(
                        LucideIcons.x), // Changer l'icône si nécessaire
                    onPressed: () => {
                          Navigator.pushReplacement(
                            context,
                            MaterialPageRoute(
                                builder: (context) => SearchPage(
                                      selectedPlantTypeList:
                                          widget.selectedPlantTypeList,
                                      selectedVille: widget.selectedVille,
                                    )),
                          )
                        }),
              ],
            ),
          ),
        ),
        body: SingleChildScrollView(
          child: Stack(
            children: [
              Container(
                padding: const EdgeInsets.symmetric(horizontal: 20),
                child: Column(
                  children: [
                    Column(
                      key: const Key('localisation_column'),
                      children: [
                        Container(
                          alignment: Alignment.centerLeft,
                          child: Text("Localisation",
                              style: ArosajeTextStyle.titleFormTextStyle),
                        ),
                        CustomTextField(
                          controller: controller,
                          color: textColor,
                          hintText: "Ville",
                          onChanged: (value) {
                            setState(() {
                              selectedVille = value;
                            });
                          },
                        )
                      ],
                    ),
                    Column(
                      children: [
                        Container(
                          alignment: Alignment.centerLeft,
                          child: Text("Types de plantes",
                              style: ArosajeTextStyle.titleFormTextStyle),
                        ),
                        Container(
                          alignment: Alignment.centerLeft,
                          child: Column(
                            children: [
                              Column(children: _buildSelectedPlantTypes()),
                              Column(children: _buildUnselectedPlantTypes()),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
        bottomNavigationBar: Padding(
          key: const Key('save_button_container'),
          padding: const EdgeInsets.only(bottom: 40, left: 20, right: 20),
          child: CustomButton(
            onPressed: () {
              Navigator.pushReplacement(
                context,
                MaterialPageRoute(
                  builder: (context) => SearchPage(
                    selectedPlantTypeList: selectedPlantTypeList!,
                    selectedVille: selectedVille,
                  ),
                ),
              );
            },
            label: 'Enregistrer',
            textColor: whiteColor,
            buttonColor: primaryColor,
            icon: LucideIcons.save,
          ),
        ),
      );
    }
  }
}
