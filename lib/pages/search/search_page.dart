import 'package:a_rosa_je/models/guard.dart';
import 'package:a_rosa_je/pages/search/search_filters_page.dart';
import 'package:a_rosa_je/services/api/data_api.dart';
import 'package:a_rosa_je/widgets/bottom_navigation_bar.dart';
import 'package:a_rosa_je/widgets/card_guard.dart';
import 'package:flutter/material.dart';
import 'package:lucide_icons/lucide_icons.dart';

import '../../theme/theme.dart';

// ignore: must_be_immutable
class SearchPage extends StatefulWidget {
  static const String routeName = "/home";

  SearchPage(
      {super.key,
      required this.selectedPlantTypeList,
      required this.selectedVille});

  List<String> selectedPlantTypeList;
  String selectedVille;

  @override
  State<SearchPage> createState() => _SearchPageState();
}

class _SearchPageState extends State<SearchPage> {
  late Map<String, dynamic> json;
  late List<Guard> guards = [];
  bool isLoading = false;
  void refresh() {
    setState(() {
      isLoading = true;
    });
    setGuardList();
  }

  @override
  void initState() {
    super.initState();
    setGuardList();
  }

  @override
  void didUpdateWidget(covariant SearchPage oldWidget) {
    super.didUpdateWidget(oldWidget);
    if (widget.selectedPlantTypeList != oldWidget.selectedPlantTypeList) {
      setGuardList();
    }
  }

  void setGuardList() async {
    setState(() {
      isLoading = true; // Commence le chargement
    });
    DataApi dataApi = DataApi();

    Future<Map<String, dynamic>> futureMap = dataApi.getGuardList(
        widget.selectedPlantTypeList, widget.selectedVille);
    json = await futureMap;
    setState(() {
      guards = json['body']['data']['guards']
          .map<Guard>((guard) => Guard.fromJson(guard))
          .toList();
    });

    setState(() {
      isLoading = false; // Commence le chargement
    });
  }

  // Guard guard = Guard.fromJson(getGuardList());

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: whiteColor,
        title: Align(
          alignment: Alignment.centerLeft,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Expanded(
                child: Text(
                  'Dernières demandes',
                  style: ArosajeTextStyle.AppBarTextStyle,
                ),
              ),
              InkWell(
                key: const Key('search_filter_button'),
                onTap: () {
                  navigateToSearchFilterPage();
                },
                child: const Icon(LucideIcons.filter),
              ),
            ],
          ),
        ),
      ),
      bottomNavigationBar: const BottomNavigationBarWidget(0),
      body: Container(
        padding: const EdgeInsets.only(right: 20, left: 20, top: 30),
        child: isLoading ? _buildLoadingIndicator() : _buildContent(),
      ),
    );
  }

  Widget _buildLoadingIndicator() {
    return const Center(
      child: CircularProgressIndicator(
          valueColor: AlwaysStoppedAnimation<Color>(Colors.green)),
    );
  }

  Widget _buildContent() {
    return guards.isEmpty
        ? const Center(child: Text('Aucun résultat'))
        : ListView.builder(
            itemCount: guards.length,
            itemBuilder: (context, index) {
              return GuardCard(
                key: ValueKey('guardCard$index'),
                guard: guards[index],
                myGuards: false,
                byCurrentUser: false,
                refreshParent: refresh,
              );
            },
          );
  }

  void navigateToSearchFilterPage() {
    Navigator.pushReplacement(
      context,
      MaterialPageRoute(
        builder: (context) => searchFiltersPage(
          selectedPlantTypeList: widget
              .selectedPlantTypeList, // Provide an empty list as default value
          selectedVille: widget.selectedVille,
        ),
      ),
    );
  }
}
