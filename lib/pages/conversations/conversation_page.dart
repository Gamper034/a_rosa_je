import 'dart:convert';
import 'dart:typed_data';

import 'package:a_rosa_je/models/guard.dart';
import 'package:a_rosa_je/models/message.dart';
import 'package:a_rosa_je/services/api/data_api.dart';
import 'package:a_rosa_je/services/user.dart';
import 'package:a_rosa_je/theme/color.dart';
import 'package:a_rosa_je/theme/textstyle.dart';
import 'package:a_rosa_je/widgets/bottom_navigation_bar.dart';
import 'package:flutter/material.dart';
import 'package:a_rosa_je/models/conversation.dart';
import 'package:intl/intl.dart';
import 'package:lucide_icons/lucide_icons.dart';

class ConversationPage extends StatefulWidget {
  final Conversation conversation;
  const ConversationPage({super.key, required this.conversation});

  @override
  State<ConversationPage> createState() => _ConversationPageState();
}

class _ConversationPageState extends State<ConversationPage> {
  late Conversation conversation;
  late List<Message> messages;
  Guard? guard;
  late Map<String, dynamic> json;
  final DataApi dataApi = DataApi();
  UserService userService = UserService();
  late DateFormat startGuard;
  late DateFormat endGuard;
  final monthNames = [
    'Janv.',
    'Fév.',
    'Mars',
    'Avr.',
    'Mai',
    'Juin',
    'Juil.',
    'août',
    'Sept.',
    'Oct.',
    'Nov.',
    'Déc.'
  ];
  final TextEditingController _textEditingController = TextEditingController();

  @override
  void initState() {
    super.initState();
    conversation = widget.conversation;
    messages = conversation.messages;

    _fetchGuard();
  }

  void _fetchGuard() async {
    var fetchedGuard = await dataApi.getGuard(conversation.guardId);
    var guardJson = fetchedGuard['body']['data']['guard'];
    Guard newGuard = Guard.fromJson(guardJson);
    setState(() {
      guard = newGuard;
    });
  }

  Future<void> _refreshConversation() async {
    var fetchedConversation =
        await dataApi.getConversationById(conversation.id, context);
    var conversationJson = fetchedConversation['body']['data']['conversation'];
    Conversation newConversation = Conversation.fromJson(conversationJson);
    setState(() {
      conversation = newConversation;
      messages = newConversation.messages; // Ensure messages are updated
    });
  }

  Widget _buildMessageTile(Message message, bool isCurrentUser) {
    return Container(
      padding: const EdgeInsets.symmetric(vertical: 10, horizontal: 10),
      // margin: const EdgeInsets.only(bottom: 1),
      child: Row(
        mainAxisAlignment:
            isCurrentUser ? MainAxisAlignment.start : MainAxisAlignment.end,
        children: [
          if (isCurrentUser) const SizedBox(width: 60),
          Expanded(
            child: Container(
              padding: const EdgeInsets.all(10),
              decoration: BoxDecoration(
                color: isCurrentUser ? secondaryColor : primaryColor,
                borderRadius: BorderRadius.circular(10),
              ),
              child: Column(
                // crossAxisAlignment: CrossAxisAlignment.start,
                // crossAxisAlignment: isCurrentUser
                //     ? CrossAxisAlignment.end
                //     : CrossAxisAlignment.end,
                children: [
                  Align(
                    alignment: Alignment.centerLeft,
                    child: Text(
                      // message.content,
                      message.content,
                      style: isCurrentUser
                          ? const TextStyle(
                              color: textColor,
                              fontSize: 16,
                            )
                          : const TextStyle(
                              color: whiteColor,
                              fontSize: 16,
                            ),
                    ),
                  ),
                  const SizedBox(height: 5),
                  Align(
                    alignment: Alignment.centerRight,
                    child: Text(
                      DateFormat('d MMM HH:mm', 'fr_FR')
                          .format(message.createdAt),
                      style: isCurrentUser
                          ? const TextStyle(fontSize: 12, color: Colors.grey)
                          : TextStyle(fontSize: 12, color: Colors.grey[300]),
                      // textAlign: TextAlign.end,
                    ),
                  ),
                ],
              ),
            ),
          ),
          if (!isCurrentUser) const SizedBox(width: 60),
        ],
      ),
    );
  }

  Widget _buildMessageInput() {
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 8.0, vertical: 10.0),
      child: Row(
        children: [
          Expanded(
            child: TextField(
              controller: _textEditingController,
              decoration: InputDecoration(
                // hintText: '',
                border: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(10.0),
                  borderSide: const BorderSide(color: Colors.grey, width: 1.0),
                ),
                enabledBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(10.0),
                  borderSide: const BorderSide(color: Colors.grey, width: 1.0),
                ),
                focusedBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(10.0),
                  borderSide: const BorderSide(color: Colors.grey, width: 1.0),
                ),
                filled: true,
                fillColor: Colors.white,
                contentPadding:
                    const EdgeInsets.symmetric(horizontal: 20.0, vertical: 5.0),
              ),
            ),
          ),
          const SizedBox(width: 8.0),
          Container(
            width: 48.0,
            height: 48.0,
            decoration: BoxDecoration(
              color: Colors.green,
              borderRadius: BorderRadius.circular(12.0), // Bords arrondis
            ),
            child: IconButton(
              icon:
                  const Icon(LucideIcons.send, color: Colors.white, size: 22.0),
              onPressed: () async {
                if (_textEditingController.text.isNotEmpty) {
                  await dataApi.addMessage(
                      conversation.id, _textEditingController.text, context);
                  _textEditingController.clear();
                  await _refreshConversation();
                }
              },
            ),
          ),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: false,
        title: Row(
          children: [
            IconButton(
                style: IconButton.styleFrom(padding: EdgeInsets.zero),
                onPressed: () {
                  Navigator.pop(context);
                },
                icon: const Icon(Icons.arrow_back_ios, color: blackColor)),
            Text("Conversation", style: ArosajeTextStyle.AppBarTextStyle)
          ],
        ),
      ),
      bottomNavigationBar: const BottomNavigationBarWidget(3),
      body: Container(
        padding: const EdgeInsets.all(20),
        child: Center(
          child: Column(
            children: [
              FutureBuilder<bool>(
                future: userService.isOwner(conversation.guard),
                builder: (context, snapshot) {
                  if (snapshot.connectionState == ConnectionState.waiting) {
                    return Container();
                  } else if (snapshot.hasError) {
                    return Text('Error: ${snapshot.error}');
                  } else {
                    final isCurrentUserOwner = snapshot.data ?? false;
                    final displayedUser = isCurrentUserOwner
                        ? conversation.guard.guardian
                        : conversation.guard.owner;
                    return Container(
                      height: 60,
                      margin: const EdgeInsets.only(top: 10, bottom: 3),
                      padding: const EdgeInsets.all(10),
                      decoration: BoxDecoration(
                        color: secondaryColor,
                        borderRadius: BorderRadius.circular(10),
                      ),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Row(
                            children: [
                              CircleAvatar(
                                radius: 16.0,
                                backgroundImage:
                                    NetworkImage(displayedUser!.avatar),
                                backgroundColor: Colors.transparent,
                              ),
                              const SizedBox(width: 10),
                              Text(
                                  "${displayedUser.firstname} ${displayedUser.lastname.substring(0, 1)}.",
                                  style: ArosajeTextStyle.titleFormTextStyle),
                            ],
                          ),
                          const Icon(
                            LucideIcons
                                .user, // Replace with desired profile icon
                            color:
                                greyColor, // Adjust color to match the design
                            size: 25.0, // Adjust size to match the design
                          ),
                        ],
                      ),
                    );
                  }
                },
              ),
              Align(
                alignment: Alignment.centerLeft,
                child:
                    Text('Garde :', style: ArosajeTextStyle.secondarySubTitle),
              ),
              Container(
                height: 60,
                margin: const EdgeInsets.only(top: 10, bottom: 10),
                padding: const EdgeInsets.only(
                    top: 5, bottom: 5, left: 5, right: 10),
                decoration: BoxDecoration(
                  color: secondaryColor,
                  borderRadius: BorderRadius.circular(10),
                ),
                child: Row(
                  children: [
                    // FutureBuilder<Uint8List>(
                    //   future:
                    //       Future.delayed(const Duration(milliseconds: 30), () {
                    //     try {
                    //       String base64String =
                    //           guard?.plants[0].image?.split(',').last.trim() ??
                    //               '';
                    //       if (base64String.isEmpty) {
                    //         throw Exception('La chaîne en base64 est vide.');
                    //       }
                    //       return base64Decode(base64String);
                    //     } catch (e) {
                    //       // Gérer l'exception, par exemple en retournant des données d'image vides ou en journalisant l'erreur
                    //       print(
                    //           'Erreur lors du décodage de l\'image en base64: $e');
                    //       return Uint8List(
                    //           0); // Retourne une liste d'entiers vide pour éviter de crasher
                    //     }
                    //   }),
                    //   builder: (context, snapshot) {
                    //     if (snapshot.connectionState ==
                    //         ConnectionState.waiting) {
                    //       return Container(); // or any other placeholder
                    //     } else if (snapshot.hasError) {
                    //       return const Icon(
                    //           Icons.error); // or any other error indicator
                    //     } else {
                    //       return ClipRRect(
                    //         borderRadius: BorderRadius.circular(5),
                    //         child: Image.memory(
                    //           snapshot.data!,
                    //           height: 50,
                    //           width: 50,
                    //           fit: BoxFit.cover,
                    //         ),
                    //       );
                    //     }
                    //   },
                    // ),
                    FutureBuilder<Uint8List>(
                      future: () async {
                        String base64String =
                            guard?.plants[0].image?.split(',').last.trim() ??
                                '';
                        if (base64String.isEmpty) {
                          throw Exception('La chaîne en base64 est vide.');
                        }

                        return base64Decode(base64String);
                      }(),
                      builder: (context, snapshot) {
                        if (snapshot.connectionState ==
                            ConnectionState.waiting) {
                          return Container(); // Indicateur de chargement
                        } else if (snapshot.hasError) {
                          return const Icon(Icons.error); // Indicateur d'erreur
                        } else if (snapshot.hasData) {
                          return ClipRRect(
                            borderRadius: BorderRadius.circular(5),
                            child: Image.memory(
                              snapshot.data!,
                              height: 50,
                              width: 50,
                              fit: BoxFit.cover,
                            ),
                          );
                        } else {
                          return Container(); // Gérer le cas où il n'y a pas de données
                        }
                      },
                    ),
                    const SizedBox(width: 10),
                    const Spacer(),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.end,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text(
                            "${conversation.guard.startDate.day} ${monthNames[conversation.guard.startDate.month - 1]} - ${conversation.guard.endDate.day} ${monthNames[conversation.guard.endDate.month - 1]}",
                            style: ArosajeTextStyle.titleFormTextStyle),
                        Text(
                            "${conversation.guard.city} ${conversation.guard.zipCode}",
                            style: ArosajeTextStyle.titleLightTextStyle),
                      ],
                    ),
                  ],
                ),
              ),
              const SizedBox(height: 30),
              Container(
                color: secondaryColor,
                height: 2,
              ),
              const SizedBox(height: 30),
              Expanded(
                child: SingleChildScrollView(
                  reverse: true,
                  child: Column(
                    children: [
                      ListView.builder(
                        shrinkWrap: true,
                        physics: const NeverScrollableScrollPhysics(),
                        itemCount: messages.length,
                        itemBuilder: (context, index) {
                          final message = messages[index];
                          return FutureBuilder<bool>(
                            future: userService.wroteTheMessage(message),
                            builder: (context, snapshot) {
                              if (snapshot.connectionState ==
                                  ConnectionState.waiting) {
                                return Container();
                              } else if (snapshot.hasError) {
                                return Text('Error: ${snapshot.error}');
                              } else {
                                final isCurrentUser = snapshot.data ?? false;
                                return _buildMessageTile(
                                    message, isCurrentUser);
                              }
                            },
                          );
                        },
                      ),
                    ],
                  ),
                ),
              ),
              _buildMessageInput(),
            ],
          ),
        ),
      ),
    );
  }
}
