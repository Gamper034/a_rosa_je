import 'package:a_rosa_je/models/conversation.dart';
import 'package:a_rosa_je/pages/conversations/conversation_page.dart';
import 'package:a_rosa_je/services/api/data_api.dart';
import 'package:a_rosa_je/services/user.dart';
import 'package:a_rosa_je/theme/color.dart';
import 'package:a_rosa_je/theme/textstyle.dart';
import 'package:a_rosa_je/widgets/bottom_navigation_bar.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class ConversationsList extends StatefulWidget {
  const ConversationsList({super.key});

  @override
  State<ConversationsList> createState() => _ConversationsListState();
}

class _ConversationsListState extends State<ConversationsList> {
  List<Conversation> conversations = [];

  @override
  void initState() {
    super.initState();
    _fetchConversations(); // Étape 2: Initialiser les conversations
  }

  void _fetchConversations() async {
    var dataApi = DataApi();
    var fetchedConversations = await dataApi.getConversationsList(context);

    // print(fetchedConversations);
    // Accéder à la liste des conversations
    var conversationsJson =
        fetchedConversations['body']['data']['conversations'] as List;

    // Convertir chaque élément de la liste en une instance de Conversation
    List<Conversation> newConversations = conversationsJson
        .map((json) => Conversation.fromJson(json as Map<String, dynamic>))
        .toList();
    setState(() {
      conversations = newConversations;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        // Change the named parameter 'margin' to 'titleSpacing'
        titleSpacing: 30,
        automaticallyImplyLeading: false,
        title: Row(
          children: [
            // IconButton(
            //     style: IconButton.styleFrom(padding: EdgeInsets.zero),
            //     onPressed: () {
            //       Navigator.pop(context);
            //     },
            //     icon: const Icon(Icons.arrow_back_ios, color: blackColor)),
            Text("Messagerie", style: ArosajeTextStyle.AppBarTextStyle)
          ],
        ),
      ),
      bottomNavigationBar: const BottomNavigationBarWidget(3),
      body: Container(
        padding: const EdgeInsets.only(top: 10, left: 20, right: 20),
        child: _conversationList(conversations),
      ),
    );
  }
}

_conversationList(conversations) {
  return ListView.builder(
    itemCount: conversations.length,
    itemBuilder: (context, index) {
      return _cardMessagerie(conversations[index]);
    },
  );
}

_cardMessagerie(Conversation conversation) {
  var startGuard =
      DateFormat('d MMM', 'fr_FR').format(conversation.guard.startDate);
  var endGuard =
      DateFormat('d MMM', 'fr_FR').format(conversation.guard.endDate);

  var userService = UserService();

  return FutureBuilder<bool>(
    future: userService.isOwner(conversation.guard),
    builder: (context, snapshot) {
      if (snapshot.connectionState == ConnectionState.waiting) {
        return const CircularProgressIndicator(); // Affiche un indicateur de chargement pendant le chargement
      } else if (snapshot.hasError) {
        return Text(
            'Erreur: ${snapshot.error}'); // Gère les erreurs potentielles
      } else {
        // Une fois les données chargées, construit l'interface utilisateur en fonction de `isOwner`
        bool isOwner = snapshot.data ??
            false; // Utilise `false` comme valeur par défaut si `data` est null

        return InkWell(
          onTap: () {
            Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) =>
                    ConversationPage(conversation: conversation),
              ),
            );
          },
          child: Container(
            margin: const EdgeInsets.all(5),
            height: 74,
            decoration: BoxDecoration(
              color: cardBackgroundColor,
              borderRadius: BorderRadius.circular(15),
            ),
            child: Row(
              children: [
                Container(
                  height: 80,
                  padding: const EdgeInsets.all(5),
                  width: 80,
                  child: ClipRRect(
                    borderRadius: BorderRadius.circular(10),
                    child: Image(
                      fit: BoxFit.cover,
                      image: NetworkImage(
                        // Remplacez par l'URL appropriée
                        isOwner
                            ? conversation.guard.guardian!.avatar
                            : conversation.guard.owner.avatar,
                      ),
                    ),
                  ),
                ),
                const SizedBox(width: 10),
                Container(
                  padding: const EdgeInsets.only(top: 10, bottom: 10),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        isOwner
                            ? "${conversation.guard.guardian!.firstname} ${conversation.guard.guardian!.lastname}"
                            : "${conversation.guard.owner.firstname} ${conversation.guard.owner.lastname}",
                        style: ArosajeTextStyle.titleFormTextStyle,
                      ),
                      const SizedBox(height: 3),
                      Text(
                        "$startGuard - $endGuard",
                        style: ArosajeTextStyle.titleLightTextStyle
                            .copyWith(height: 0.9),
                      ),
                      const SizedBox(height: 2),
                      Text(
                        "${conversation.guard.city}, ${conversation.guard.zipCode}",
                        style: ArosajeTextStyle.titleLightTextStyle
                            .copyWith(height: 0.9),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        );
      }
    },
  );
}
