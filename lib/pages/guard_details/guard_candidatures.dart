import 'package:a_rosa_je/models/application.dart';
import 'package:a_rosa_je/models/guard.dart';
import 'package:a_rosa_je/services/api/data_api.dart';
import 'package:a_rosa_je/widgets/toast_info.dart';
import 'package:flutter/material.dart';
import 'package:lucide_icons/lucide_icons.dart';

import '../../theme/theme.dart';

class GuardCandidature extends StatefulWidget {
  const GuardCandidature({super.key, required this.guard});
  final Guard guard;

  @override
  State<GuardCandidature> createState() => _GuardCandidatureState();
}

class _GuardCandidatureState extends State<GuardCandidature> {
  late List<Application>? applications;
  late Guard guard;
  DataApi dataApi = DataApi();

  final monthNames = [
    'Janv.',
    'Fév.',
    'Mars',
    'Avr.',
    'Mai',
    'Juin',
    'Juil.',
    'août',
    'Sept.',
    'Oct.',
    'Nov.',
    'Déc.'
  ];

  @override
  void initState() {
    guard = widget.guard;
    applications = widget.guard.applications;
    super.initState();
  }

  Container applicantCard(Application application) {
    return Container(
      height: 70,
      margin: const EdgeInsets.only(top: 10, bottom: 20),
      padding: const EdgeInsets.all(10),
      decoration: BoxDecoration(
        color: secondaryColor,
        borderRadius: BorderRadius.circular(10),
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Row(
            children: [
              CircleAvatar(
                radius: 16.0,
                backgroundImage: NetworkImage(application.userAvatar),
                backgroundColor: const Color.fromRGBO(0, 0, 0, 0),
              ),
              const SizedBox(width: 10),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    "${application.userFirstName} ${application.userLastName.substring(0, 1)}.",
                    style: const TextStyle(color: textColor, fontSize: 16),
                  ),
                  Text(
                    application.nbGuardsDonebyUser.toString() +
                        (application.nbGuardsDonebyUser < 2
                            ? " garde effectuée"
                            : " gardes effectuées"),
                    style: const TextStyle(color: Colors.black45),
                  ),
                ],
              ),
            ],
          ),
          Row(
            children: [
              GestureDetector(
                onTap: () {
                  dataApi.removeApply(application.id);
                  setState(() {
                    guard.applications!.remove(application);
                  });
                },
                child: Container(
                  width: 40,
                  height: 40,
                  decoration: BoxDecoration(
                    color: Colors.red,
                    borderRadius: BorderRadius.circular(10),
                  ),
                  margin: const EdgeInsets.symmetric(horizontal: 5),
                  child: const Icon(
                    LucideIcons.x,
                    color: whiteColor,
                  ),
                ),
              ),
              GestureDetector(
                onTap: () {
                  dataApi.confirmApply(application.id);
                  setState(() {
                    Application temp = application;
                    guard.applications!.clear();
                    guard.applications!.add(temp);
                  });
                  _dialogDone(context);
                },
                child: Container(
                  width: 40,
                  height: 40,
                  decoration: BoxDecoration(
                    color: primaryColor,
                    borderRadius: BorderRadius.circular(10),
                  ),
                  margin: const EdgeInsets.symmetric(horizontal: 5),
                  child: const Icon(
                    LucideIcons.check,
                    color: whiteColor,
                  ),
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: false,
        title: Row(
          children: [
            IconButton(
                style: IconButton.styleFrom(padding: EdgeInsets.zero),
                onPressed: () {
                  Navigator.pop(context);
                },
                icon: const Icon(Icons.arrow_back_ios, color: blackColor)),
            Text("Candidatures", style: ArosajeTextStyle.AppBarTextStyle)
          ],
        ),
      ),
      body: Container(
        padding: const EdgeInsets.all(20),
        child: Center(
          child: Column(
            children: [
              const Align(
                alignment: Alignment.centerLeft,
                child: Text(
                  'Garde :',
                  style: TextStyle(
                    color: textColor,
                  ),
                ),
              ),
              Container(
                height: 60,
                margin: const EdgeInsets.only(top: 10, bottom: 20),
                padding: const EdgeInsets.all(10),
                decoration: BoxDecoration(
                  color: secondaryColor,
                  borderRadius: BorderRadius.circular(10),
                ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Row(
                      children: [
                        CircleAvatar(
                          radius: 16.0,
                          backgroundImage: NetworkImage(guard.owner.avatar),
                          backgroundColor: Colors.transparent,
                        ),
                        const SizedBox(width: 10),
                        Text(
                          "${guard.owner.firstname} ${guard.owner.lastname.substring(0, 1)}.",
                          style:
                              const TextStyle(color: textColor, fontSize: 16),
                        ),
                      ],
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.end,
                      children: [
                        Text(
                          "${guard.startDate.day} ${monthNames[guard.startDate.month - 1]} - ${guard.endDate.day} ${monthNames[guard.endDate.month - 1]}",
                          style: const TextStyle(
                              color: textColor,
                              fontSize: 16,
                              fontWeight: FontWeight.w500),
                        ),
                        Text(
                          "${guard.city} ${guard.zipCode}",
                          style: const TextStyle(
                              color: secondaryTextColor,
                              fontSize: 12,
                              fontWeight: FontWeight.w400),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
              const SizedBox(height: 20),
              Container(
                color: secondaryColor,
                height: 2,
              ),
              const SizedBox(height: 20),
              Expanded(
                // Utiliser Expanded pour prendre l'espace restant
                child: SingleChildScrollView(
                  // Permettre le défilement pour le contenu suivant
                  child: Column(
                    children: [
                      if (applications == null || applications!.isEmpty)
                        const Text('Aucune candidature pour le moment'),
                      if (applications != null && applications!.isNotEmpty)
                        for (var application in applications!)
                          applicantCard(application),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  _dialogDone(BuildContext context) {
    return showDialog<void>(
      context: context,
      builder: (BuildContext context) {
        return Dialog(
          child: ToastInfo(
            icon: LucideIcons.badgeCheck,
            title: "Candidature confirmée",
            content: "Le gardien a bien été sélectionné.",
            onPressedConfirm: () {
              Navigator.of(context).pop();
              Navigator.of(context).pop();
            },
            height: 240,
            theme: "primary",
          ),
        );
      },
    );
  }
}
