// ignore_for_file: use_build_context_synchronously

import 'dart:convert';

import 'package:a_rosa_je/models/conversation.dart';
import 'package:a_rosa_je/models/user.dart';
import 'package:a_rosa_je/pages/conversations/conversation_page.dart';
import 'package:a_rosa_je/pages/guard_details/guard_candidatures.dart';
import 'package:a_rosa_je/models/advice.dart';
import 'package:a_rosa_je/models/visit.dart';
import 'package:a_rosa_je/pages/advices/guard/botanist_guard_advices.dart';
import 'package:a_rosa_je/pages/visits/guard_visits.dart';
import 'package:a_rosa_je/services/api/data_api.dart';
import 'package:a_rosa_je/services/guard.dart';
import 'package:a_rosa_je/services/user.dart';
import 'package:a_rosa_je/widgets/status_badge_guard/status_badge.dart';
import 'package:a_rosa_je/widgets/widgets.dart';
import 'package:flutter/material.dart';
import 'package:lucide_icons/lucide_icons.dart';
import 'package:a_rosa_je/models/guard.dart';

import '../../theme/theme.dart';

class GuardDetails extends StatefulWidget {
  const GuardDetails({super.key, required this.guard, required this.user});
  final Guard guard;
  final User user;

  @override
  State<GuardDetails> createState() => _GuardDetailsState();
}

class _GuardDetailsState extends State<GuardDetails> {
  List<Advice> advices = [];
  late List<Visit> visitsList;
  late List<Visit> visits;
  late User user;
  Conversation? conversation;
  bool isLoadingConversation = false;

  late Map<String, dynamic> json;
  Guard? guard;
  late GuardService guardService;
  late GuardStatus status;
  DataApi dataApi = DataApi();

  @override
  void initState() {
    user = widget.user;
    guardService = GuardService(user: user);
    // print(user.id);
    super.initState();
    setGuard();
  }

  Future<void> _fetchConversation(Guard guard) async {
    isLoadingConversation = true;
    if (guard.conversationId == null) {
      conversation = null;
    } else {
      if (guard.conversationId == "") {
        conversation = null;
      } else {
        var fetchedConversation =
            await dataApi.getConversationById(guard.conversationId!, context);
        var conversationJson =
            fetchedConversation['body']['data']['conversation'];
        Conversation newConversation = Conversation.fromJson(conversationJson);
        setState(() {
          conversation = newConversation;
        });
      }
    }
    isLoadingConversation = false;
  }

  void setGuard() async {
    Future<Map<String, dynamic>> futureMap = dataApi.getGuard(widget.guard.id);
    json = await futureMap;
    Guard newGuard = Guard.fromJson(json['body']['data']['guard']);
    await _fetchConversation(newGuard);
    setState(() {
      guard = newGuard;
    });
  }

  @override
  Widget build(BuildContext context) {
    if (guard == null) {
      return Container(
        color: Colors.white,
        child: const Center(
          child: CircularProgressIndicator(
            color: primaryColor,
          ),
        ),
      );
    } else {
      status = guardService.getStatus(guard!);
      return Scaffold(
        body: Container(
          color: Colors.white,
          child: SingleChildScrollView(
            child: Column(
              children: [
                Stack(
                  children: [
                    Image(
                      height: 300,
                      width: double.infinity,
                      fit: BoxFit.cover,
                      image: MemoryImage(
                        base64Decode(
                            guard!.plants[0].image?.split(',').last ?? ''),
                      ),
                    ),
                    Positioned(
                      top: 60,
                      left: 20,
                      child: Container(
                        decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(5),
                        ),
                        height: 40,
                        width: 40,
                        child: IconButton(
                          onPressed: () {
                            Navigator.pop(context);
                          },
                          icon: const Icon(
                            LucideIcons.chevronLeft,
                            color: Colors.black,
                            size: 20,
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
                Padding(
                  padding:
                      const EdgeInsets.symmetric(horizontal: 20, vertical: 20),
                  child: Column(
                    key: const Key('guardDetailsColumn'),
                    children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Align(
                            alignment: Alignment.centerLeft,
                            child: StatusBadge.getBadgeStatus(status),
                          ),
                          if (status == GuardStatus.enCours)
                            _infoGardeEnCours(),
                        ],
                      ),
                      //Badge de validation
                      const SizedBox(height: 10),
                      if (status != GuardStatus.enCours)
                        _infoStatutNotEnCours(),
                      if ((status == GuardStatus.enCours &&
                              guard!.guardian != null) ||
                          (status == GuardStatus.aVenir &&
                              guard!.guardian != null))
                        _guardianInfo(),
                      if (status != GuardStatus.enAttente &&
                          status != GuardStatus.aVenir &&
                          guard!.guardian != null)
                        _btnVisites(),
                      if (((status == GuardStatus.aVenir &&
                                  guard!.guardian == null) ||
                              (status == GuardStatus.enCours &&
                                  guard!.guardian == null)) &&
                          guard!.owner.id != user.id)
                        _btnPostuler(),
                      if ((status == GuardStatus.enAttente ||
                              status == GuardStatus.enCours) &&
                          guard!.owner.id == user.id &&
                          guard!.guardian == null)
                        _btnCandidature(),
                      const SizedBox(height: 20),
                      CustomButton(
                        onPressed: () async {
                          advices = [];
                          DataApi dataApi = DataApi();

                          Future<Map<String, dynamic>> futureMap =
                              dataApi.getGuardAdvices(context, guard!.id);
                          Map<String, dynamic> json = await futureMap;
                          // print(await futureMap);
                          List<dynamic> jsonAdvices =
                              json['body']['data']['advices'];
                          // print(jsonAdvices);
                          // advices = jsonAdvices
                          //     .map((advice) => Advice.fromJson(advice))
                          //     .toList();
                          for (var advice in jsonAdvices) {
                            advices.add(Advice.fromJson(advice));
                          }

                          //Récupération des informations de l'utilisateur connecté
                          var isBotanist = await UserService().isBotanist();

                          Navigator.push(
                            context,
                            MaterialPageRoute(
                              builder: (context) => BotanistGuardAdvices(
                                  guard: guard!,
                                  advices: advices,
                                  isBotanist: isBotanist),
                            ),
                          );
                        },
                        label: 'Conseils de Botanistes',
                        key: const Key('botanistAdviceButton'),
                        textColor: textColor,
                        buttonColor: Colors.white,
                        border: true,
                        icon: LucideIcons.flower2,
                      ),
                      const SizedBox(height: 50),
                      const Align(
                        alignment: Alignment.centerLeft,
                        child: Text(
                          'Plantes à garder',
                          style: TextStyle(
                              color: Colors.black,
                              fontSize: 16,
                              fontWeight: FontWeight.w500),
                        ),
                      ),

                      Column(
                        key: const Key('plantList'),
                        children: [
                          for (var plant in guard!.plants)
                            CardPlant(
                              plant: plant,
                            ),
                        ],
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      );
    }
  }

  _dialogConfirm(BuildContext context) {
    return showDialog<void>(
      context: context,
      builder: (BuildContext context) {
        return Dialog(
          child: ToastConfirm(
            icon: LucideIcons.helpCircle,
            title: "Postuler pour cette garde",
            content: "Êtes-vous sûr de vouloir postuler pour cette garde ?",
            onPressedConfirm: () async {
              var applyGuard = await dataApi.applyToGuard(guard!.id);
              if (applyGuard['statusCode'] == 201) {
                _dialogDone(context);
              } else {
                _dialogError(context);
              }
            },
            onPressedCancel: () {
              Navigator.of(context).pop();
            },
            height: 270,
          ),
        );
      },
    );
  }

  _dialogDone(BuildContext context) {
    return showDialog<void>(
      context: context,
      builder: (BuildContext context) {
        return Dialog(
          child: ToastInfo(
            icon: LucideIcons.badgeCheck,
            title: "Candidature envoyée",
            content: "Votre candidature a été envoyée.",
            onPressedConfirm: () {
              Navigator.of(context).pop();
              Navigator.of(context).pop();
              setState(() {
                setGuard();
                status = guardService.getStatus(guard!);
              });
            },
            height: 240,
            theme: "primary",
          ),
        );
      },
    );
  }

  _dialogAlreadyApplied(BuildContext context) {
    return showDialog<void>(
      context: context,
      builder: (BuildContext context) {
        return Dialog(
          child: ToastInfo(
            icon: LucideIcons.badgeCheck,
            title: "Déjà postulé",
            content: "Votre candidature a déjà été prise en compte !",
            onPressedConfirm: () {
              Navigator.of(context).pop();
            },
            height: 250,
            theme: "primary",
          ),
        );
      },
    );
  }

  _dialogError(BuildContext context) {
    return showDialog<void>(
      context: context,
      builder: (BuildContext context) {
        return Dialog(
          child: ToastError(
            icon: LucideIcons.badgeX,
            title: "Impossible de postuler",
            content: "un problème est survenu, veuillez réessayer.",
            onPressedConfirm: () {
              // WidgetsBinding.instance.addPostFrameCallback((_) {
              Navigator.of(context).pop();
              Navigator.of(context).pop();
              // });
            },
            height: 250,
          ),
        );
      },
    );
  }

  bool _checkIfAlreadyApplied() {
    if (guard!.applications!.isNotEmpty) {
      for (var candidature in guard!.applications!) {
        if (candidature.userId == user.id) {
          return false;
        }
      }
    }
    return true;
  }

  Container _guardianInfo() {
    return Container(
      decoration: BoxDecoration(
        color: secondaryColor,
        borderRadius: BorderRadius.circular(10),
      ),
      padding: const EdgeInsets.all(10),
      margin: const EdgeInsets.only(top: 10),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          const Text(
            "Gardien :",
            style: TextStyle(
                color: textColor, fontSize: 14, fontWeight: FontWeight.w500),
          ),
          Row(
            children: [
              const SizedBox(width: 10),
              Text(
                "${guard!.guardian!.firstname} ${guard!.guardian!.lastname.substring(0, 1)}.",
                style: ArosajeTextStyle.labelFormTextStyle,
              ),
              const SizedBox(width: 10),
              CircleAvatar(
                radius: 16.0,
                backgroundImage: NetworkImage(guard!.guardian!.avatar),
                backgroundColor: Colors.transparent,
              ),
            ],
          ),
        ],
      ),
    );
  }

  Align _infoGardeEnCours() {
    return Align(
      alignment: Alignment.centerRight,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.end,
        children: [
          Text(
            "${guard!.startDate.day} ${GuardService.monthNames[guard!.startDate.month - 1]} - ${guard!.endDate.day} ${GuardService.monthNames[guard!.endDate.month - 1]}",
            style: const TextStyle(
                color: textColor, fontSize: 16, fontWeight: FontWeight.w500),
          ),
          Text(
            "${guard!.city} ${guard!.zipCode}",
            style: const TextStyle(
                color: secondaryTextColor,
                fontSize: 12,
                fontWeight: FontWeight.w400),
          ),
        ],
      ),
    );
  }

  Row _infoStatutNotEnCours() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Row(
          children: [
            CircleAvatar(
              radius: 16.0,
              backgroundImage: NetworkImage(guard!.owner.avatar),
              backgroundColor: Colors.transparent,
            ),
            const SizedBox(width: 10),
            Text(
              "${guard!.owner.firstname} ${guard!.owner.lastname.substring(0, 1)}.",
              style: const TextStyle(color: textColor, fontSize: 16),
            ),
          ],
        ),
        Column(
          crossAxisAlignment: CrossAxisAlignment.end,
          children: [
            Text(
              "${guard!.startDate.day} ${GuardService.monthNames[guard!.startDate.month - 1]} - ${guard!.endDate.day} ${GuardService.monthNames[guard!.endDate.month - 1]}",
              style: const TextStyle(
                  color: textColor, fontSize: 16, fontWeight: FontWeight.w500),
            ),
            Text(
              "${guard!.city} ${guard!.zipCode}",
              style: const TextStyle(
                  color: secondaryTextColor,
                  fontSize: 12,
                  fontWeight: FontWeight.w400),
            ),
          ],
        ),
      ],
    );
  }

  Column _btnVisites() {
    return Column(
      children: [
        const SizedBox(height: 30),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Expanded(
              child: Padding(
                padding: const EdgeInsets.only(right: 5),
                child: CustomButton(
                  onPressed: () async {
                    visits = [];
                    DataApi dataApi = DataApi();

                    Future<Map<String, dynamic>> futureMap =
                        dataApi.getGuardVisits(context, guard!.id);
                    Map<String, dynamic> json = await futureMap;
                    // print(json);
                    List<dynamic> jsonVisits = json['body']['data']['visits'];
                    // print(jsonVisits);
                    // visits = jsonVisits
                    //     .map((visit) => Visit.fromJson(visit))
                    //     .toList();

                    if (jsonVisits != [])
                      // ignore: curly_braces_in_flow_control_structures
                      for (var visit in jsonVisits) {
                        visits.add(Visit.fromJson(visit));
                      }
                    var isGuardianOfGuard =
                        await GuardService().isUserConnectedGuardianOf(guard!);

                    Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => GuardVisitList(
                          guard: guard!,
                          visits: visits,
                          isGuardianOfGuard: isGuardianOfGuard,
                        ),
                      ),
                    );
                  },
                  label: 'Visites',
                  buttonColor: primaryColor,
                  textColor: Colors.white,
                ),
              ),
            ),
            Expanded(
              child: Padding(
                padding: const EdgeInsets.only(left: 5),
                child: CustomButton(
                  onPressed: () {
                    if (conversation != null) {
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) =>
                              ConversationPage(conversation: conversation!),
                        ),
                      );
                    } else {}
                  },
                  label: isLoadingConversation
                      ? 'Chargement...'
                      : (conversation != null ? 'Messages' : 'Pas de Conv'),
                  buttonColor: secondaryColor,
                  textColor: textColor,
                ),
              ),
            )
          ],
        ),
      ],
    );
  }

  Column _btnPostuler() {
    return Column(
      children: [
        const SizedBox(height: 20),
        if (!_checkIfAlreadyApplied())
          CustomButton(
            onPressed: () {
              _dialogAlreadyApplied(context);
            },
            label: 'Candidature envoyée',
            textColor: Colors.white,
            buttonColor: Colors.grey[400]!,
          ),
        if (_checkIfAlreadyApplied())
          CustomButton(
            onPressed: () {
              _dialogConfirm(context);
            },
            label: 'Postuler',
            textColor: Colors.white,
            buttonColor: primaryColor,
          ),
      ],
    );
  }

  Column _btnCandidature() {
    return Column(
      children: [
        const SizedBox(height: 20),
        CustomButton(
          onPressed: () {
            Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) => GuardCandidature(guard: guard!),
              ),
            ).then((_) => setGuard());
            setState(() {
              setGuard();
              status = guardService.getStatus(guard!);
            });
          },
          label: 'Candidatures',
          textColor: Colors.white,
          buttonColor: primaryColor,
        ),
      ],
    );
  }
}
