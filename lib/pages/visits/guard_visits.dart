// ignore_for_file: use_build_context_synchronously

import 'package:a_rosa_je/models/guard.dart';
import 'package:a_rosa_je/models/visit.dart';
import 'package:a_rosa_je/pages/visits/new_visit.dart';
import 'package:a_rosa_je/pages/visits/visit_detail.dart';
import 'package:a_rosa_je/services/api/data_api.dart';
import 'package:a_rosa_je/services/guard.dart';
import 'package:a_rosa_je/theme/theme.dart';
import 'package:a_rosa_je/widgets/widgets.dart';
import 'package:flutter/material.dart';

class GuardVisitList extends StatefulWidget {
  final Guard guard;
  final List<Visit> visits;
  final bool isGuardianOfGuard;

  const GuardVisitList(
      {super.key,
      required this.guard,
      required this.visits,
      required this.isGuardianOfGuard});

  @override
  State<GuardVisitList> createState() => _BotanistAdvicesState();
}

class _BotanistAdvicesState extends State<GuardVisitList> {
  late List<Visit> visits;
  late Guard guard;
  late Map<String, dynamic> json;
  late bool isGuardianOfGuard;

  @override
  void initState() {
    guard = widget.guard;
    visits = widget.visits;
    isGuardianOfGuard = widget.isGuardianOfGuard;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: false,
        title: Row(
          children: [
            IconButton(
                style: IconButton.styleFrom(padding: EdgeInsets.zero),
                onPressed: () {
                  Navigator.pop(context);
                },
                icon: const Icon(Icons.arrow_back_ios, color: blackColor)),
            Text("Liste des visites", style: ArosajeTextStyle.AppBarTextStyle)
          ],
        ),
      ),
      body: Container(
        padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 30),
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text('Garde:', style: ArosajeTextStyle.titleLightTextStyle),
              const SizedBox(height: 10),
              Container(
                decoration: BoxDecoration(
                  color: secondaryColor,
                  borderRadius: BorderRadius.circular(10),
                ),
                padding:
                    const EdgeInsets.symmetric(horizontal: 10, vertical: 10),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Row(
                      children: [
                        CircleAvatar(
                          radius: 16.0,
                          backgroundImage: NetworkImage(guard.owner.avatar),
                          backgroundColor: Colors.transparent,
                        ),
                        const SizedBox(width: 10),
                        Text(
                          "${guard.owner.firstname} ${guard.owner.lastname.substring(0, 1)}.",
                          style:
                              const TextStyle(color: textColor, fontSize: 16),
                        ),
                      ],
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.end,
                      children: [
                        Text(
                          "${widget.guard.startDate.day} ${GuardService.monthNames[guard.startDate.month - 1]} - ${guard.endDate.day} ${GuardService.monthNames[guard.endDate.month - 1]}",
                          style: const TextStyle(
                              color: textColor,
                              fontSize: 16,
                              fontWeight: FontWeight.w500),
                        ),
                        Text(
                          "${guard.city} ${guard.zipCode}",
                          style: const TextStyle(
                              color: greyColor,
                              fontSize: 12,
                              fontWeight: FontWeight.w400),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
              const SizedBox(height: 30),
              const Divider(
                color: Colors.grey,
                height: 20,
              ),
              const SizedBox(height: 30),
              if (isGuardianOfGuard == true)
                CustomButton(
                  onPressed: () => {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => NewVisit(
                          guard: guard,
                        ),
                      ),
                    ).then(
                      (_) => _getVisits(),
                    )
                  },
                  label: 'Ajouter une nouvelle visite',
                  buttonColor: primaryColor,
                  textColor: whiteColor,
                ),
              const SizedBox(height: 30),
              if (visits.isNotEmpty) _visitsList() else _noVisit(),
            ],
          ),
        ),
      ),
    );
  }

  _noVisit() {
    return Container(
      padding: const EdgeInsets.symmetric(vertical: 50),
      child: Align(
        alignment: Alignment.center,
        child: Text('Aucune visite pour le moment.',
            style: ArosajeTextStyle.regularGreyTextStyle),
      ),
    );
  }

  _visitsList() {
    return ListView.builder(
      shrinkWrap: true,
      itemCount: visits.length,
      itemBuilder: (context, index) {
        String date =
            "${visits[index].date.day} ${GuardService.monthNames[visits[index].date.month - 1]}";
        String indexString = (index + 1).toString();

        return _visitItem(date, indexString, visits[index]);
      },
    );
  }

  _visitItem(String date, String index, Visit visit) {
    return GestureDetector(
      onTap: () async {
        DataApi dataApi = DataApi();

        Future<Map<String, dynamic>> futureMap =
            dataApi.getVisit(context, visit.id);
        Map<String, dynamic> json = await futureMap;
        // print(await futureMap);
        Map<String, dynamic> jsonVisit = json['body']['data']['visit'];
        // print(jsonVisit);
        visit = Visit.fromJson(jsonVisit);

        Navigator.push(
          context,
          MaterialPageRoute(
            builder: (context) => VisitDetail(
              visit: visit,
              guard: guard,
            ),
          ),
        ).then(
          (_) => _getVisits(),
        );
      },
      child: Container(
        margin: const EdgeInsets.only(bottom: 20),
        padding: const EdgeInsets.all(20),
        decoration: BoxDecoration(
          color: backgroundContainer,
          borderRadius: BorderRadius.circular(10),
        ),
        height: 60,
        child: Row(
          children: [
            Text(
              '$index#',
              style: ArosajeTextStyle.secondarySubTitle,
            ),
            const SizedBox(width: 10),
            Text(
              date,
              style: ArosajeTextStyle.titleFormTextStyle,
            )
          ],
        ),
      ),
    );
  }

  _getVisits() async {
    visits = [];
    DataApi dataApi = DataApi();

    Future<Map<String, dynamic>> futureMap =
        dataApi.getGuardVisits(context, guard.id);
    Map<String, dynamic> json = await futureMap;
    List<dynamic> jsonVisits = json['body']['data']['visits'];
    for (var visit in jsonVisits) {
      visits.add(Visit.fromJson(visit));
    }
    setState(() {});
  }
}
