// ignore_for_file: use_super_parameters

import 'dart:convert';

import 'package:a_rosa_je/models/advice.dart';
import 'package:a_rosa_je/models/guard.dart';
import 'package:a_rosa_je/models/plant_visit.dart';
import 'package:a_rosa_je/models/visit.dart';
import 'package:a_rosa_je/pages/advices/visit/botanist_visit_advices.dart';
import 'package:a_rosa_je/services/api/data_api.dart';
import 'package:a_rosa_je/services/guard.dart';
import 'package:a_rosa_je/services/user.dart';
import 'package:a_rosa_je/widgets/widgets.dart';
import 'package:flutter/material.dart';
import 'package:lucide_icons/lucide_icons.dart';

import '../../theme/theme.dart';

class VisitDetail extends StatefulWidget {
  final Visit visit;
  final Guard guard;
  const VisitDetail({Key? key, required this.visit, required this.guard})
      : super(key: key);

  @override
  State<VisitDetail> createState() => _VisitDetailState();
}

class _VisitDetailState extends State<VisitDetail> {
  late Visit visit;
  List<Advice> advices = [];

  late Guard guard;
  late List<PlantVisit> plantsVisit;
  bool isBotanist = false;

  @override
  void initState() {
    visit = widget.visit;
    guard = widget.guard;
    plantsVisit = visit.plants;
    //print(plantsVisit.length);
    // print(visit);

    super.initState();
    _loadAdvicesData();
  }

  @override
  Widget build(BuildContext context) {
    var day = visit.date.day == 1 ? "${visit.date.day}er" : visit.date.day;
    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: false,
        title: Row(
          children: [
            IconButton(
                style: IconButton.styleFrom(padding: EdgeInsets.zero),
                onPressed: () {
                  Navigator.pop(context);
                },
                icon: const Icon(Icons.arrow_back_ios, color: blackColor)),
            Text(
                'Visite - $day ${GuardService.fullMonthNames[visit.date.month - 1]}',
                style: ArosajeTextStyle.AppBarTextStyle)
          ],
        ),
      ),
      body: SingleChildScrollView(
        child: Container(
          padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 30),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                decoration: BoxDecoration(
                  color: secondaryColor,
                  borderRadius: BorderRadius.circular(10),
                ),
                padding:
                    const EdgeInsets.symmetric(horizontal: 10, vertical: 10),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Row(
                      children: [
                        CircleAvatar(
                          radius: 16.0,
                          backgroundImage: NetworkImage(guard.guardian!.avatar),
                          backgroundColor: Colors.transparent,
                        ),
                        const SizedBox(width: 10),
                        Text(
                          "${guard.guardian!.firstname} ${guard.guardian!.lastname.substring(0, 1)}.",
                          style:
                              const TextStyle(color: textColor, fontSize: 16),
                        ),
                      ],
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.end,
                      children: [
                        Text(
                          "${widget.guard.startDate.day} ${GuardService.monthNames[guard.startDate.month - 1]} - ${guard.endDate.day} ${GuardService.monthNames[guard.endDate.month - 1]}",
                          style: const TextStyle(
                              color: textColor,
                              fontSize: 16,
                              fontWeight: FontWeight.w500),
                        ),
                        Text(
                          "${guard.city} ${guard.zipCode}",
                          style: const TextStyle(
                              color: greyColor,
                              fontSize: 12,
                              fontWeight: FontWeight.w400),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
              const SizedBox(height: 30),
              CustomButton(
                onPressed: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => BotanistVisitAdvices(
                        visit: visit,
                        advices: advices,
                        guard: guard,
                        isBotanist: isBotanist,
                      ),
                    ),
                  );
                },
                label: 'Conseils de Botanistes',
                textColor: textColor,
                buttonColor: whiteColor,
                border: true,
                icon: LucideIcons.flower2,
              ),
              const SizedBox(height: 30),
              const Divider(
                color: Colors.grey,
                height: 20,
              ),
              const SizedBox(height: 30),
              Text(
                'Commentaire',
                style: ArosajeTextStyle.titleFormTextStyle,
              ),
              const SizedBox(height: 5),
              Text(
                visit.comment ?? "Aucun commentaire",
                style: ArosajeTextStyle.contentSecondaryTextStyle,
              ),
              const SizedBox(height: 30),
              _buildPlant(),
            ],
          ),
        ),
      ),
    );
  }

  _buildPlant() {
    return Column(
      children: [
        for (var i = 0; i < plantsVisit.length; i++) _plantItem(plantsVisit[i]),
      ],
    );
  }

  _plantItem(PlantVisit plant) {
    // print(plant);
    return Container(
      padding: const EdgeInsets.all(5),
      margin: const EdgeInsets.only(bottom: 15),
      decoration: BoxDecoration(
        color: Colors.grey[100],
        borderRadius: BorderRadius.circular(10),
      ),
      height: 85,
      child: Row(
        children: [
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text(plant.plantInfo.name,
                    style: ArosajeTextStyle.titleFormTextStyle),
                Text(plant.plantInfo.plantType,
                    style: ArosajeTextStyle.labelFormTextStyle),
              ],
            ),
          ),
          ClipRRect(
            borderRadius: BorderRadius.circular(10),
            child: SizedBox(
              width: 66,
              height: 66,
              child: Image(
                fit: BoxFit.cover,
                image: MemoryImage(
                  base64Decode(
                    plant.image.split(',').last.trim(),
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  _loadAdvicesData() async {
    advices = [];
    DataApi dataApi = DataApi();

    Future<Map<String, dynamic>> futureMap =
        dataApi.getVisitAdvices(context, visit.id);
    Map<String, dynamic> json = await futureMap;
    // print(await futureMap);
    List<dynamic> jsonAdvices = json['body']['data']['advices'];
    // print(jsonAdvices);
    // advices = jsonAdvices
    //     .map((advice) => Advice.fromJson(advice))
    //     .toList();
    for (var advice in jsonAdvices) {
      advices.add(Advice.fromJson(advice));
    }

    isBotanist = await UserService().isBotanist();
  }
}
