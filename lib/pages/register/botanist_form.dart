import 'package:a_rosa_je/services/api/data_api.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:a_rosa_je/widgets/button.dart';
import 'package:a_rosa_je/widgets/text_field.dart';
import 'package:a_rosa_je/theme/theme.dart';

// ignore: use_key_in_widget_constructors
class BotanistForm extends StatefulWidget {
  @override
  // ignore: library_private_types_in_public_api
  _BotanistFormState createState() => _BotanistFormState();
}

class _BotanistFormState extends State<BotanistForm> {
  final _formKey = GlobalKey<FormState>();
  String _firstname = '';
  String _lastname = '';
  String _email = '';
  String _siret = '';
  String _password = '';
  final String _role = 'botanist';
  String errorMessage = '';
  bool _checkboxCGU = false;

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Form(
        key: _formKey,
        child: Column(
          children: <Widget>[
            CustomTextField(
              color: textColor,
              hintText: "Prénom",
              onSaved: (value) => _firstname = value ?? '',
              validator: (value) =>
                  value?.isEmpty ?? true ? 'Ce champ est obligatoire' : null,
            ),
            CustomTextField(
              color: textColor,
              hintText: "Nom",
              onSaved: (value) => _lastname = value ?? '',
              validator: (value) =>
                  value?.isEmpty ?? true ? 'Ce champ est obligatoire' : null,
            ),
            CustomTextField(
              color: textColor,
              hintText: "Email",
              onSaved: (value) => _email = value ?? '',
              validator: (value) =>
                  value?.isEmpty ?? true ? 'Ce champ est obligatoire' : null,
            ),
            CustomTextField(
              color: textColor,
              hintText: "N° de SIRET",
              onSaved: (value) => _siret = value ?? '',
              validator: (value) =>
                  value?.isEmpty ?? true ? 'Ce champ est obligatoire' : null,
            ),
            CustomTextField(
              color: textColor,
              hintText: "Mot de passe",
              obscureText: true,
              onSaved: (value) => _password = value ?? '',
              validator: (value) =>
                  value?.isEmpty ?? true ? 'Ce champ est obligatoire' : null,
            ),
            const SizedBox(height: 5),
            Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                Checkbox(
                  checkColor: whiteColor,
                  activeColor: primaryColor,
                  value: _checkboxCGU,
                  onChanged: (bool? value) {
                    setState(() {
                      _checkboxCGU = value ?? false;
                    });
                  },
                ),
                Expanded(
                  child: RichText(
                    text: TextSpan(
                      text: "Accepter les ",
                      style: const TextStyle(
                        color: textColor,
                        fontWeight: FontWeight.w400,
                        fontSize: 12,
                      ),
                      children: <TextSpan>[
                        TextSpan(
                          text: 'conditions générales d\'utilisation',
                          style: const TextStyle(
                            fontWeight: FontWeight.w500,
                            color: textColor,
                            decoration: TextDecoration.underline,
                            fontSize: 12,
                          ),
                          recognizer: TapGestureRecognizer()
                            ..onTap = () {
                              Navigator.pushNamed(context, '/cgu');
                            },
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
            const SizedBox(height: 5),
            CustomButton(
              onPressed: _submit,
              label: 'S\'inscrire',
              buttonColor: primaryColor,
              textColor: whiteColor,
            ),
            Padding(
              padding: const EdgeInsets.only(top: 1),
              child: Text(
                errorMessage,
                style: const TextStyle(color: Colors.red),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Future<void> _submit() async {
    if (_formKey.currentState?.validate() ?? false) {
      _formKey.currentState?.save();
      if (!_checkboxCGU) {
        setState(() {
          errorMessage = 'Vous devez accepter les CGU';
        });
        return;
      }
      setState(() {
        errorMessage = '';
      }); // Sauvegarde les valeurs des champs
      DataApi user = DataApi();
      String result = await user.registerBotanist(
        context,
        _role,
        _firstname,
        _lastname,
        _email,
        _siret,
        _password,
      );

      setState(() {
        errorMessage = result;
      });
    }
  }
}
