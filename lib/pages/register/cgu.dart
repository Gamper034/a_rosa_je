import 'package:flutter/material.dart';

import '../../theme/theme.dart';

class CguPage extends StatelessWidget {
  const CguPage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.grey[200],
          automaticallyImplyLeading: false,
          title: Row(
            children: [
              IconButton(
                  style: IconButton.styleFrom(padding: EdgeInsets.zero),
                  onPressed: () {
                    Navigator.pop(context);
                  },
                  icon: const Icon(Icons.arrow_back_ios, color: blackColor)),
              Text("CGU", style: ArosajeTextStyle.AppBarTextStyle)
            ],
          ),
        ),
        body: SingleChildScrollView(
          child: Padding(
            padding:
                const EdgeInsets.only(left: 20, right: 20, top: 20, bottom: 40),
            child: Column(
              children: [
                Align(
                  alignment: Alignment.centerLeft,
                  child: Text(
                    "Conditions Générales d'Utilisation (CGU)",
                    style: ArosajeTextStyle.titleFormTextStyle,
                  ),
                ),
                const SizedBox(height: 5),
                const Text(
                  "Date d'entrée en vigueur : 23/02/2024\n\nBienvenue sur l'application A'Rosa-je.\n\nVeuillez lire attentivement ces Conditions Générales d'Utilisation avant d'utiliser A'Rosa-je. En accédant ou en utilisant l'Application, vous acceptez d'être lié par ces CGU.",
                  textAlign: TextAlign.justify,
                ),
                const SizedBox(height: 25),
                Align(
                  alignment: Alignment.centerLeft,
                  child: Text(
                    "1. Collecte et Utilisation des Données Personnelles",
                    style: ArosajeTextStyle.titleFormTextStyle,
                  ),
                ),
                const SizedBox(height: 10),
                const Text(
                  "En utilisant l'Application, vous consentez à la collecte et à l'utilisation de vos données personnelles conformément à notre Politique de Confidentialité. Nous nous engageons à respecter les dispositions du RGPD en matière de protection des données personnelles.",
                  textAlign: TextAlign.justify,
                ),
                const SizedBox(height: 25),
                Align(
                  alignment: Alignment.centerLeft,
                  child: Text(
                    "2. Sécurité des Données",
                    style: ArosajeTextStyle.titleFormTextStyle,
                  ),
                ),
                const SizedBox(height: 10),
                const Text(
                  "Nous mettons en place les mesures de sécurité appropriées pour protéger vos données personnelles contre tout accès non autorisé, toute divulgation, toute altération ou toute destruction non autorisée.",
                  textAlign: TextAlign.justify,
                ),
                const SizedBox(height: 25),
                Align(
                  alignment: Alignment.centerLeft,
                  child: Text(
                    "3. Droits des Utilisateurs",
                    style: ArosajeTextStyle.titleFormTextStyle,
                  ),
                ),
                const SizedBox(height: 10),
                const Text(
                  "Conformément au RGPD, vous avez le droit d'accéder à vos données personnelles, de demander leur rectification, leur suppression ou leur portabilité. Vous avez également le droit de vous opposer au traitement de vos données personnelles dans certaines circonstances. Pour exercer vos droits, veuillez nous contacter à admin@arosaje.com.",
                  textAlign: TextAlign.justify,
                ),
                const SizedBox(height: 25),
                Align(
                  alignment: Alignment.centerLeft,
                  child: Text(
                    "4. Durée de Conservation des Données",
                    style: ArosajeTextStyle.titleFormTextStyle,
                  ),
                ),
                const SizedBox(height: 10),
                const Text(
                  "Nous conservons vos données personnelles aussi longtemps que nécessaire pour fournir les services demandés ou pour remplir nos obligations légales, contractuelles ou réglementaires.",
                  textAlign: TextAlign.justify,
                ),
                const SizedBox(height: 25),
                Align(
                  alignment: Alignment.centerLeft,
                  child: Text(
                    "5. Cookies",
                    style: ArosajeTextStyle.titleFormTextStyle,
                  ),
                ),
                const SizedBox(height: 10),
                const Text(
                  "Nous utilisons des cookies pour améliorer votre expérience utilisateur. En utilisant l'Application, vous consentez à l'utilisation de cookies conformément à notre Politique en matière de cookies.",
                  textAlign: TextAlign.justify,
                ),
                const SizedBox(height: 25),
                Align(
                  alignment: Alignment.centerLeft,
                  child: Text(
                    "6. Modification des CGU",
                    style: ArosajeTextStyle.titleFormTextStyle,
                  ),
                ),
                const SizedBox(height: 10),
                const Text(
                  "Nous nous réservons le droit de modifier ces CGU à tout moment. Toute modification sera publiée sur cette page. Votre utilisation continue de l'Application après la publication des modifications constitue votre acceptation de ces modifications.",
                  textAlign: TextAlign.justify,
                ),
                const SizedBox(height: 25),
                Align(
                  alignment: Alignment.centerLeft,
                  child: Text(
                    "7. Contact",
                    style: ArosajeTextStyle.titleFormTextStyle,
                  ),
                ),
                const SizedBox(height: 10),
                const Text(
                  "Si vous avez des questions ou des préoccupations concernant ces CGU ou notre Politique de Confidentialité, veuillez nous contacter à admin@arosaje.com.\nEn utilisant l'Application, vous acceptez les présentes CGU. Si vous n'acceptez pas ces CGU, veuillez ne pas utiliser l'Application.",
                  textAlign: TextAlign.justify,
                ),
              ],
            ),
          ),
        ));
  }
}
