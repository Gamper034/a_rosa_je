// ignore_for_file: use_key_in_widget_constructors, library_private_types_in_public_api

import 'package:flutter/material.dart';
import 'package:a_rosa_je/widgets/button.dart';
import 'package:a_rosa_je/pages/login/login_page.dart';
import 'package:flutter/gestures.dart';
import 'package:a_rosa_je/pages/register/user_form.dart';
import 'package:a_rosa_je/pages/register/botanist_form.dart';
import 'package:a_rosa_je/theme/theme.dart';

class RegisterPage extends StatefulWidget {
  @override
  _RegisterPageState createState() => _RegisterPageState();
}

class _RegisterPageState extends State<RegisterPage> {
  bool isUserForm = true;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Center(
          child: Padding(
            padding: const EdgeInsets.all(20.0),
            child: Column(
              children: [
                Padding(
                  padding: const EdgeInsets.only(
                    top: 80,
                    bottom: 30,
                  ),
                  child: SizedBox(
                    width: 110,
                    height: 110,
                    child: Image.asset(
                        'assets/images/logos/png/logo_vert_noir.png'),
                  ),
                ),

                Container(
                  alignment: Alignment.centerLeft,
                  child: Text('Je suis:',
                      style: ArosajeTextStyle.titleLightTextStyle),
                ),
                const SizedBox(height: 10),
                Row(children: [
                  Expanded(
                    child: CustomButton(
                      buttonColor: isUserForm ? primaryColor : secondaryColor,
                      textColor: isUserForm ? whiteColor : textColor,
                      onPressed: () {
                        setState(() {
                          isUserForm = true;
                        });
                      },
                      label: 'Utilisateur',
                    ),
                  ),
                  const SizedBox(width: 10),
                  Expanded(
                    child: CustomButton(
                      buttonColor: isUserForm ? secondaryColor : primaryColor,
                      textColor: isUserForm ? textColor : whiteColor,
                      onPressed: () {
                        setState(() {
                          isUserForm = false;
                        });
                      },
                      label: 'Botaniste',
                    ),
                  ),
                ]),
                const SizedBox(height: 5),
                const Divider(
                  color: Colors.grey,
                  height: 20,
                ),
                const SizedBox(height: 5),

                // ...

                isUserForm ? UserForm() : BotanistForm(),

                Padding(
                  padding: const EdgeInsets.only(top: 10),
                  child: RichText(
                    text: TextSpan(
                      text: 'Déjà inscrit ? ',
                      style: const TextStyle(
                          color: textColor, fontWeight: FontWeight.w500),
                      children: <TextSpan>[
                        TextSpan(
                          text: 'Se connecter',
                          style: const TextStyle(
                            fontWeight: FontWeight.w500,
                            color: primaryColor,
                            decoration: TextDecoration.underline,
                          ),
                          recognizer: TapGestureRecognizer()
                            ..onTap = () {
                              Navigator.pushAndRemoveUntil(
                                context,
                                MaterialPageRoute(
                                  builder: (context) => LoginPage(),
                                ),
                                (route) => false,
                              );
                            },
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
