// ignore_for_file: use_build_context_synchronously

import 'dart:io';

import 'package:a_rosa_je/models/user.dart';
import 'package:a_rosa_je/pages/login/login_page.dart';
import 'package:a_rosa_je/pages/register/confirm_sign_up.dart';
import 'package:a_rosa_je/services/user.dart';
import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:flutter/foundation.dart' show kIsWeb, TargetPlatform;
import 'package:flutter/foundation.dart' show defaultTargetPlatform;

class DataApi {
  final storage = const FlutterSecureStorage();

  static String getHost() {
    if (kIsWeb) {
      return 'localhost';
    }
    switch (defaultTargetPlatform) {
      case TargetPlatform.android:
        return '10.0.2.2';
      case TargetPlatform.iOS:
        return 'localhost';
      default:
        return 'localhost';
    }
  }

  Future<String> registerUser(BuildContext context, String role,
      String firstname, String lastname, String email, String password) async {
    try {
      final Map<String, dynamic> registrationData = {
        'role': role,
        'firstname': firstname,
        'lastname': lastname,
        'email': email,
        'password': password,
      };

      final response = await http.post(
        Uri.parse('http://${getHost()}:2000/user/register'),
        headers: <String, String>{
          'Content-Type': 'application/json; charset=UTF-8',
        },
        body: jsonEncode(registrationData),
      );

      String errorMessage;
      var json = jsonDecode(response.body);

      if (response.statusCode == 201) {
        Navigator.pushAndRemoveUntil(
          context,
          MaterialPageRoute(builder: (context) => LoginPage()),
          (route) => false,
        );
        errorMessage = '';
      } else if (response.statusCode == 400 &&
          json['message'] == 'User already exists') {
        errorMessage = 'L\'utilisateur existe déjà.';
      } else if (response.statusCode == 400 &&
          json['message'] == "Invalid email") {
        errorMessage = 'L\'email est invalide.';
      } else {
        // print('Response body: ${json}');

        errorMessage = 'Une erreur est survenue.';
      }

      return errorMessage;
    } catch (e) {
      throw Exception('Failed to register user: $e');
    }
  }

  Future<String> registerBotanist(
      BuildContext context,
      String role,
      String firstname,
      String lastname,
      String email,
      String siret,
      String password) async {
    String errorMessage;
    try {
      final Map<String, dynamic> registrationData = {
        'role': role,
        'firstname': firstname,
        'lastname': lastname,
        'email': email,
        'siret': siret,
        'password': password,
      };

      final response = await http.post(
        Uri.parse('http://${getHost()}:2000/user/register'),
        headers: <String, String>{
          'Content-Type': 'application/json; charset=UTF-8',
        },
        body: jsonEncode(registrationData),
      );

      var json = jsonDecode(response.body);

      if (response.statusCode == 201) {
        Navigator.pushAndRemoveUntil(
          context,
          MaterialPageRoute(builder: (context) => const ConfirmSignUp()),
          (route) => false,
        );
        errorMessage = '';
      } else if (response.statusCode == 400 &&
          json['message'] == 'User already exists') {
        errorMessage = 'Le botaniste existe déjà.';
      } else if (response.statusCode == 400 &&
          json['message'] == "Invalid email") {
        errorMessage = 'L\'email est invalide.';
      } else {
        // print('Response body: ${json['message']}');

        errorMessage = 'Une erreur est survenue.';
      }

      return errorMessage;
    } catch (e) {
      throw Exception('Failed to register user: $e');
    }
  }

  Future<Map<String, dynamic>> getGuardList(
      List<String> selectedPlantTypeList, String selectedVille) async {
    try {
      Map<String, dynamic> filters;

      filters = {
        'filters': {
          "city": selectedVille,
          "plantTypes": selectedPlantTypeList,
        },
      };

      String? jwt = await storage.read(key: 'jwt');

      final response = await http.post(
        Uri.parse('http://${getHost()}:2000/guard/availables'),
        headers: <String, String>{
          'Content-Type': 'application/json; charset=UTF-8',
          'Cookie': '$jwt',
        },
        body: jsonEncode(filters),
      );

      return {
        'statusCode': response.statusCode,
        'body': jsonDecode(response.body),
      };
    } catch (e) {
      throw Exception('Failed to get guard list: $e');
    }
  }

  // Future<Map<String, dynamic>> addGuard(
  //     BuildContext context,
  //     String startDate,
  //     String endDate,
  //     // String address,
  //     String zipCode,
  //     String city,
  //     List plants) async {
  //   try {
  //     String? jwt = await storage.read(key: 'jwt');
  //     var headers = {
  //       'Cookie': '$jwt',
  //     };
  //     // print(jwt);
  //     var request = http.MultipartRequest(
  //         'POST', Uri.parse('http://${getHost()}:2000/guard/add'));

  //     // request.fields.addAll({
  //     //   'startDate': startDate,
  //     //   'endDate': endDate,
  //     //   // 'address': address,
  //     //   'zipCode': zipCode,
  //     //   'city': city,
  //     // });
  //     // Convertir le tableau plants en une structure attendue avec images en base64
  //     List<Map<String, dynamic>> plantsData =
  //         await Future.wait(plants.map((plant) async {
  //       return {
  //         'type': plant['selectedPlantType'],
  //         'name': plant['plantName'],
  //         'image':
  //             "data:image/jpeg;base64,${await convertImageFileToBase64(plant['plantImageUrl'])}",
  //       };
  //     }).toList());

  //     // Ajouter les champs normaux
  //     request.fields.addAll({
  //       'startDate': startDate,
  //       'endDate': endDate,
  //       'zipCode': zipCode,
  //       'city': city,
  //       'plants': jsonEncode(
  //           plantsData), // Ajouter le tableau plants comme un champ JSON
  //     });

  //     request.headers.addAll(headers);

  //     http.StreamedResponse streamedResponse = await request.send();
  //     http.Response response = await http.Response.fromStream(streamedResponse);
  //     print(response.body);
  //     if (response.statusCode == 401) {
  //       UserService.logout(context);
  //     }
  //     return {
  //       'statusCode': response.statusCode,
  //       'body': jsonDecode(response.body),
  //     };
  //   } catch (e) {
  //     throw Exception('Failed to add guard: $e');
  //   }
  // }

  Future<Map<String, dynamic>> addMessage(
      String conversationId, String content, BuildContext context) async {
    try {
      String? jwt = await storage.read(key: 'jwt');

      // Création de l'objet JSON pour le corps de la requête
      Map<String, dynamic> body = {
        'conversationId': conversationId,
        'content': content,
      };

      String jsonBody = jsonEncode(body);

      http.Request request = http.Request('POST',
          Uri.parse('http://${getHost()}:2000/conversation/addMessage'));
      request.body = jsonBody;
      request.headers.addAll({
        'Content-Type': 'application/json',
        'Cookie': '$jwt',
      });

      http.StreamedResponse streamedResponse = await request.send();
      http.Response response = await http.Response.fromStream(streamedResponse);
      // print(response.body);
      if (response.statusCode == 401) {
        UserService.logout(context);
      } else if (response.statusCode != 201 && response.statusCode != 200) {
        throw Exception(
            'Failed to add message with status code: ${response.statusCode}');
      }
      return {
        'statusCode': response.statusCode,
        'body': jsonDecode(response.body),
      };
    } catch (e) {
      throw Exception('Failed to add Message: $e');
    }
  }

  Future<Map<String, dynamic>> addGuard(
      String startDate,
      String endDate,
      String zipCode,
      String city,
      List<Map<String, dynamic>> plants,
      // Map<String, String> headers,
      BuildContext context) async {
    try {
      String? jwt = await storage.read(key: 'jwt');

      // Convertir chaque plante en base64 et construire plantsData
      List<Map<String, dynamic>> plantsData =
          await Future.wait(plants.map((plant) async {
        return {
          'type': plant['selectedPlantType'],
          'name': plant['plantName'],
          'image': await convertImageFileToBase64(plant['plantImageUrl']),
        };
      }).toList());

      // Création de l'objet JSON pour le corps de la requête
      Map<String, dynamic> body = {
        'startDate': startDate,
        'endDate': endDate,
        'zipCode': zipCode,
        'city': city,
        'plants': plantsData, // Utiliser directement plantsData
      };

      String jsonBody = jsonEncode(body);

      http.Request request =
          http.Request('POST', Uri.parse('http://${getHost()}:2000/guard/add'));
      request.body = jsonBody;
      request.headers.addAll({
        'Content-Type': 'application/json',
        'Cookie': '$jwt',
      });

      http.StreamedResponse streamedResponse = await request.send();
      http.Response response = await http.Response.fromStream(streamedResponse);
      // print(response.body);
      if (response.statusCode == 401) {
        UserService.logout(context);
      } else if (response.statusCode != 201) {
        throw Exception(
            'Failed to add guard with status code: ${response.statusCode}');
      }
      return {
        'statusCode': response.statusCode,
        'body': jsonDecode(response.body),
      };
    } catch (e) {
      throw Exception('Failed to add guard: $e');
    }
  }

  Future<Map<String, dynamic>> getPlantTypeList() async {
    String? jwt = await storage.read(key: 'jwt');

    final response = await http.get(
      Uri.parse('http://${getHost()}:2000/plant/getTypes'),
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
        'Cookie': '$jwt',
      },
    );
    if (response.statusCode == 200) {
      var jsonResponse = json.decode(response.body);

      if (jsonResponse['status'] == 'success' &&
          jsonResponse['data']['plantTypes'] != null) {
        List<dynamic> plantTypes = jsonResponse['data']['plantTypes'];
        List<String> plantTypeNames =
            plantTypes.map((type) => type['name'].toString()).toList();

        // Créer un Map pour encapsuler la liste des noms
        Map<String, dynamic> result = {'plantTypeNames': plantTypeNames};
        return result;
      } else {
        throw Exception(
            'La réponse de l\'API ne contient pas les types de plantes attendus.');
      }
    } else {
      throw Exception(
          'Échec de la requête API avec le code de statut: ${response.statusCode}');
    }
  }

  Future<List<String>> getPlantsType() async {
    try {
      final response = await http.get(
        Uri.parse('http://${getHost()}:2000/plant/getTypes'),
        headers: <String, String>{
          'Content-Type': 'application/json; charset=UTF-8',
        },
      );

      List<String> plantsType = [];

      if (response.statusCode == 200) {
        var body = jsonDecode(response.body);
        for (var plant in body) {
          plantsType.add(plant['name']);
        }
      }

      return plantsType;
    } catch (e) {
      throw Exception('Failed to get plants type: $e');
    }
  }

  Future<Map<String, dynamic>> getOwnerGuards() async {
    //Récupérer l'id de l'utilisateur
    UserService userService = UserService();
    User owner = await userService.getUserPreferences();

    //Récupérer le jwt
    String? jwt = await storage.read(key: 'jwt');

    var request = http.Request(
        'GET', Uri.parse('http://${getHost()}:2000/guard/user/${owner.id}'));

    request.headers['Cookie'] = '$jwt';

    http.StreamedResponse response = await request.send();

    return {
      'statusCode': response.statusCode,
      'body': jsonDecode(await response.stream.bytesToString()),
    };
    // return {
    //   'statusCode': response.statusCode,
    //   'body': jsonDecode(response()),
    // };
  }

  Future<Map<String, dynamic>> applyToGuard(String guardId) async {
    try {
      String? jwt = await storage.read(key: 'jwt');

      final response = await http.get(
        Uri.parse('http://${getHost()}:2000/guard/apply/$guardId'),
        headers: <String, String>{
          'Content-Type': 'application/json; charset=UTF-8',
          'Cookie': '$jwt',
        },
      );

      // print(response.statusCode);
      // print(response.body);

      return {
        'statusCode': response.statusCode,
        'body': jsonDecode(response.body),
      };
    } catch (e) {
      throw Exception('Failed to get guard list: $e');
    }
  }

  Future<Map<String, dynamic>> removeApply(String applicationId) async {
    try {
      String? jwt = await storage.read(key: 'jwt');

      final response = await http.get(
        Uri.parse('http://${getHost()}:2000/guard/apply/remove/$applicationId'),
        headers: <String, String>{
          'Content-Type': 'application/json; charset=UTF-8',
          'Cookie': '$jwt',
        },
      );

      return {
        'statusCode': response.statusCode,
        'body': jsonDecode(response.body),
      };
    } catch (e) {
      throw Exception('Failed to get guard list: $e');
    }
  }

  Future<Map<String, dynamic>> confirmApply(String applicationId) async {
    try {
      String? jwt = await storage.read(key: 'jwt');

      final response = await http.get(
        Uri.parse(
            'http://${getHost()}:2000/guard/apply/confirm/$applicationId'),
        headers: <String, String>{
          'Content-Type': 'application/json; charset=UTF-8',
          'Cookie': '$jwt',
        },
      );

      return {
        'statusCode': response.statusCode,
        'body': jsonDecode(response.body),
      };
    } catch (e) {
      throw Exception('Failed to get guard list: $e');
    }
  }

  Future<Map<String, dynamic>> getGuard(String guardId) async {
    try {
      String? jwt = await storage.read(key: 'jwt');

      final response = await http.get(
        Uri.parse('http://${getHost()}:2000/guard/detail/$guardId'),
        headers: <String, String>{
          'Content-Type': 'application/json; charset=UTF-8',
          'Cookie': '$jwt',
        },
      );

      return {
        'statusCode': response.statusCode,
        'body': jsonDecode(response.body),
      };
    } catch (e) {
      throw Exception('Failed to get guard list: $e');
    }
  }

  Future<Map<String, dynamic>> getGuardAdvices(
      BuildContext context, guardId) async {
    //Récupérer le jwt
    String? jwt = await storage.read(key: 'jwt');

    final response = await http.get(
      Uri.parse('http://${getHost()}:2000/advice/guard/$guardId'),
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
        'Cookie': '$jwt',
      },
    );

    if (response.statusCode == 401) {
      UserService.logout(context);
    }

    return {
      'statusCode': response.statusCode,
      'body': jsonDecode(response.body),
    };
  }

  Future<Map<String, dynamic>> publishGuardAdvice(
      String guardId, String content) async {
    String? jwt = await storage.read(key: 'jwt');

    var headers = {
      'Content-Type': 'application/json',
      'Cookie': '$jwt',
    };
    var request = http.Request(
        'POST', Uri.parse('http://${getHost()}:2000/advice/guard/add'));
    request.body = json.encode({"guardId": guardId, "content": content});
    request.headers.addAll(headers);

    http.StreamedResponse response = await request.send();
    // print(response.statusCode);
    return {
      'statusCode': response.statusCode,
    };
  }

  Future<Map<String, dynamic>> publishVisitAdvice(
      String visitId, String content) async {
    String? jwt = await storage.read(key: 'jwt');

    var headers = {
      'Content-Type': 'application/json',
      'Cookie': '$jwt',
    };
    var request = http.Request(
        'POST', Uri.parse('http://${getHost()}:2000/advice/visit/add'));
    request.body = json.encode({"visitId": visitId, "content": content});
    request.headers.addAll(headers);

    http.StreamedResponse response = await request.send();
    // print(response.statusCode);
    return {
      'statusCode': response.statusCode,
    };
  }

  Future<Map<String, dynamic>> getGuardVisits(
      BuildContext context, guardId) async {
    String? jwt = await storage.read(key: 'jwt');
    // print(jwt);

    final response = await http.get(
      Uri.parse('http://${getHost()}:2000/visit/guard/$guardId'),
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
        'Cookie': '$jwt',
      },
    );

    if (response.statusCode == 401) {
      UserService.logout(context);
    }

    return {
      'statusCode': response.statusCode,
      'body': jsonDecode(response.body),
    };
  }

  Future<Map<String, dynamic>> addVisit(BuildContext context, String visitDate,
      String comment, List plantImages, String guardId) async {
    String? jwt = await storage.read(key: 'jwt');

    List<String> plantImagesBase64 = [];

    for (var imagePath in plantImages) {
      String base64Image = await convertImageFileToBase64(imagePath);
      plantImagesBase64.add(base64Image);
    }

    Map<String, dynamic> requestBody = {
      'date': visitDate,
      'comment': comment,
      'plants': plantImagesBase64,
    };

    String jsonBody = jsonEncode(requestBody);

    http.Request request = http.Request(
        'POST', Uri.parse('http://${getHost()}:2000/visit/add/$guardId'));
    request.body = jsonBody;
    request.headers.addAll({
      'Content-Type': 'application/json',
      'Cookie': '$jwt',
    });

    http.StreamedResponse streamedResponse = await request.send();
    http.Response response = await http.Response.fromStream(streamedResponse);
    // print(response.body);
    if (response.statusCode == 401) {
      UserService.logout(context);
    }

    return {
      'statusCode': response.statusCode,
      'body': jsonDecode(response.body),
    };
  }

  Future<Map<String, dynamic>> getVisit(
      BuildContext context, String visitId) async {
    String? jwt = await storage.read(key: 'jwt');
    // print(jwt);

    final response = await http.get(
      Uri.parse('http://${getHost()}:2000/visit/$visitId'),
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
        'Cookie': '$jwt',
      },
    );

    if (response.statusCode == 401) {
      UserService.logout(context);
    }
    return {
      'statusCode': response.statusCode,
      'body': jsonDecode(response.body),
    };
  }

  Future<Map<String, dynamic>> getVisitAdvices(
      BuildContext context, String visitId) async {
    String? jwt = await storage.read(key: 'jwt');
    // print(jwt);

    final response = await http.get(
      Uri.parse('http://${getHost()}:2000/advice/visit/$visitId'),
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
        'Cookie': '$jwt',
      },
    );

    if (response.statusCode == 401) {
      UserService.logout(context);
    }
    return {
      'statusCode': response.statusCode,
      'body': jsonDecode(response.body),
    };
  }

  Future<Map<String, dynamic>> getConversationsList(
      BuildContext context) async {
    String? jwt = await storage.read(key: 'jwt');
    // print(jwt);

    final response = await http.get(
      Uri.parse('http://${getHost()}:2000/conversation/all'),
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
        'Cookie': '$jwt',
      },
    );

    if (response.statusCode == 401) {
      UserService.logout(context);
    }
    return {
      'statusCode': response.statusCode,
      'body': jsonDecode(response.body),
    };
  }

  Future<Map<String, dynamic>> getConversationById(
      String conversationId, BuildContext context) async {
    String? jwt = await storage.read(key: 'jwt');
    // print(jwt);

    final response = await http.get(
      Uri.parse('http://${getHost()}:2000/conversation/$conversationId'),
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
        'Cookie': '$jwt',
      },
    );

    if (response.statusCode == 401) {
      UserService.logout(context);
    }
    return {
      'statusCode': response.statusCode,
      'body': jsonDecode(response.body),
    };
  }
}

// Fonction pour convertir une image en base64
Future<String> convertImageFileToBase64(String imagePath) async {
  File imageFile = File(imagePath);
  List<int> imageBytes = await imageFile.readAsBytes();
  return "data:image/jpeg;base64,${base64Encode(imageBytes)}";
}
