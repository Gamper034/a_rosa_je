import 'package:flutter/material.dart';

const Color primaryColor = Color(0xFF4CAE4F);
const Color primaryLightColor = Color(0xFF80C684);
const Color secondaryColor = Color(0xFFEEEEEE);
const Color textColor = Color(0xFF4A4A4A);
const Color solidGreen = Color(0xFF2F7F33);
const Color secondaryTextColor = Color(0xFF9B9B9B);
const Color greenSolid = Color(0xFF0B1E0B);
const Color blueBadge = Color(0xFF15AFE0);
const Color warningColor = Color(0xFFF6B60E);
const Color greyColor = Color(0xFF727272);
const Color contentColor = Color(0xFF1E1E1E);
const Color backgroundContainer = Color(0xFFF5F5F5);
const Color whiteColor = Color(0xFFFFFFFF);
const Color blackColor = Color(0xFF000000);
const Color cardBackgroundColor = Color(0xFFF5F5F5);
