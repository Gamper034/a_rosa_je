import 'package:a_rosa_je/models/guard.dart';
import 'package:a_rosa_je/models/message.dart';

class Conversation {
  String id;
  String guardId;
  Guard guard;
  // List<Message>? messages = [];
  DateTime createdAt;
  List<Message> messages;

  Conversation({
    required this.id,
    required this.guardId,
    required this.guard,
    // this.messages,
    required this.createdAt,
    required this.messages,
  });

  factory Conversation.fromJson(Map<String, dynamic> json) {
    Conversation conversation = Conversation(
      id: json['id'],
      guardId: json['guardId'],
      guard: Guard.fromJson(json['guard']),
      createdAt: DateTime.parse(json['createdAt']),
      messages: List<Message>.from(
          (json['messages'] as List).map((item) => Message.fromJson(item))),
    );
    // Sort the messages by date, from oldest to newest
    conversation.messages.sort((a, b) => a.createdAt.compareTo(b.createdAt));

    return conversation;
  }
}
