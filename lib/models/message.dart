class Message {
  String id;
  String conversationId;
  String userId;
  String content;
  DateTime createdAt;

  Message({
    required this.id,
    required this.conversationId,
    required this.userId,
    required this.content,
    required this.createdAt,
  });

  static fromJson(message) {
    return Message(
      id: message['id'],
      conversationId: message['conversationId'],
      userId: message['userId'],
      content: message['content'],
      createdAt: DateTime.parse(message['createdAt']),
    );
  }
}
