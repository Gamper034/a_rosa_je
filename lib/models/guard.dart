import 'package:a_rosa_je/models/advice.dart';
import 'package:a_rosa_je/models/application.dart';
import 'package:a_rosa_je/models/plant.dart';
import 'package:a_rosa_je/models/user.dart';

class Guard {
  String id;
  String? ownerId;
  User owner;
  DateTime startDate;
  DateTime endDate;
  // String address;
  String zipCode;
  String city;
  String? guardianId;
  User? guardian;
  List<Application>? applications;
  List<Plant> plants;
  // List<Visit>? visits;
  List<Advice>? advices;
  String? conversationId;
  DateTime createdAt;

  Guard({
    required this.id,
    this.ownerId,
    required this.owner,
    required this.startDate,
    required this.endDate,
    // required this.address,
    required this.zipCode,
    required this.city,
    this.guardianId,
    this.applications,
    this.guardian,
    required this.plants,
    // this.visits,
    this.advices,
    this.conversationId,
    required this.createdAt,
  });

  factory Guard.fromJson(Map<String, dynamic> json) {
    return Guard(
      id: json['id'],
      ownerId: json['ownerId'],
      owner: User.fromJson(json['owner']),
      startDate: DateTime.parse(json['startDate']),
      endDate: DateTime.parse(json['endDate']),
      // address: json['address'],
      zipCode: json['zipCode'],
      city: json['city'],
      guardianId: json['guardianId'],
      applications: json["applications"] != null
          ? json["applications"]
              .map<Application>(
                  (application) => Application.fromJson(application))
              .toList()
          : [],
      guardian:
          json['guardian'] != null ? User.fromJson(json['guardian']) : null,
      plants: json['plants'] != null
          ? json['plants'].map<Plant>((item) => Plant.fromJson(item)).toList()
          : [],
      // visits: json['visits'] != null
      //     ? json['visits'].map<Visit>((item) => Visit.fromJson(item)).toList()
      //     : [],
      advices: [],
      conversationId:
          json['conversation'] != null ? json['conversation']['id'] : null,
      createdAt: DateTime.parse(json['createdAt']),
    );
  }

  @override
  String toString() {
    return 'Guard{id: $id, owner: $owner, startDate: $startDate, endDate: $endDate, zipCode: $zipCode, city: $city, applicants: $applications, guardian: $guardian, plants: $plants, advices: $advices, conversation: $conversationId, createdAt: $createdAt}';
  }
}
