import 'package:a_rosa_je/pages/login/login_page.dart';
import 'package:a_rosa_je/pages/register/cgu.dart';
import 'package:a_rosa_je/pages/search/search_page.dart';
import 'package:a_rosa_je/pages/splash_screen.dart';
import 'package:flutter/material.dart';
import 'package:a_rosa_je/pages/register/confirm_sign_up.dart';
import 'package:a_rosa_je/pages/register/register_page.dart';
import 'package:intl/date_symbol_data_local.dart';
import 'package:flutter_localizations/flutter_localizations.dart';

void main() {
  runApp(
    MaterialApp(
      debugShowCheckedModeBanner: false,
      // home: SplashScreen(),
      initialRoute: '/loading',
      routes: {
        '/loading': (context) => SplashScreen(),
        '/login': (context) => LoginPage(),
        '/cgu': (context) => const CguPage(),
        '/confirmSignUp': (context) => const ConfirmSignUp(),
        '/signup': (context) => RegisterPage(),
        '/home': (context) => SearchPage(
              selectedPlantTypeList: const [],
              selectedVille: "",
            ),
        // SearchPage.routeName: (context) => SearchPage(
        //       selectedPlantTypeList: const [],
        //       selectedVille: "",
        //     ),
        // ProfilPage.routeName: (context) => const ProfilPage(),
        // MyGuards.routeName: (context) => const MyGuards(),
        // PublishGuard.routeName: (context) => const PublishGuard(),
      },
      supportedLocales: const [
        Locale('fr', ''), // Français
        // Autres locales...
      ],
      localizationsDelegates: const [
        GlobalMaterialLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate,
        GlobalCupertinoLocalizations.delegate,
      ],
    ),
  );

  initializeDateFormatting('fr_FR', null);
}
