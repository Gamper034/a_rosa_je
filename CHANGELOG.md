# Changelog

Toutes les modifications notables de ce projet seront documentées dans ce fichier.

Le format est basé sur [Keep a Changelog](https://keepachangelog.com/fr/1.0.0/),
et ce projet adhère à [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [0.1.0] - 2024-02-22
### Ajouté
- Premier workpackage de l'application.
- Ajout des fonctionnalités suivantes:
    - Création de compte Utilisateur.
    - Création de compte Botaniste.
    - Authentification
    - Possibilité D'ajouter une garde.
    - Consulter les gardes disponibles.
    - Consulter une garde.
    - Consulter ses gardes publiées et les gardes auxquelles vous avez postulé.
    - Postuler à une garde disponible.
    - Accepter une candidature sur une garde postée
    - Ajouter des visites sur une garde.
    - Ajout de conseils sur les gardes & les visites en tant que botaniste.
    - Page profil.

## [1.0] - 2024-07-09
### Ajouté
- Dernier workpackage.
- Ajout des fonctionnalités suivantes:
    - Messagerie utilisateur
    - Gestion des images en base64

