// Importez les packages nécessaires
import 'package:flutter_test/flutter_test.dart';
import 'package:flutter/material.dart';
import 'package:a_rosa_je/widgets/text_field.dart';

void main() {
  testWidgets('CustomTextField correctly displays hintText',
      (WidgetTester tester) async {
    // Créez le widget
    await tester.pumpWidget(const MaterialApp(
      home: Scaffold(
        body: Column(
          children: <CustomTextField>[
            CustomTextField(
              hintText: 'Texte 1',
              color: Colors.blue,
            ),
            CustomTextField(
              hintText: 'Texte 2',
              color: Colors.red,
            ),
            CustomTextField(
              hintText: 'Texte 3',
              color: Colors.green,
            ),
            CustomTextField(
              hintText: 'Texte 4',
              color: Colors.yellow,
            ),
          ],
        ),
      ),
    ));

    // Vérifiez que les textes d'indice sont affichés correctement
    expect(find.text('Texte 1'), findsOneWidget);
    expect(find.text('Texte 2'), findsOneWidget);
    expect(find.text('Texte 3'), findsOneWidget);
    expect(find.text('Texte 4'), findsOneWidget);
  });

  //Testing the CustomTextField with obscureText
  testWidgets('CustomTextField correctly displays obscureText',
      (WidgetTester tester) async {
    // Créez le widget
    await tester.pumpWidget(const MaterialApp(
      home: Scaffold(
        body: CustomTextField(
          hintText: 'Mot de passe',
          obscureText: true,
          color: Colors.blue,
        ),
      ),
    ));

    // Vérifiez que le texte d'indice est affiché correctement
    expect(find.text('Mot de passe'), findsOneWidget);
  });

  //Testing the CustomTextField with errorColor
  testWidgets('CustomTextField correctly displays errorColor',
      (WidgetTester tester) async {
    // Créez le widget
    await tester.pumpWidget(const MaterialApp(
      home: Scaffold(
        body: CustomTextField(
          hintText: 'Texte 1',
          color: Colors.blue,
          errorColor: Colors.red,
        ),
      ),
    ));

    // Vérifiez que le texte d'indice est affiché correctement
    expect(find.text('Texte 1'), findsOneWidget);
  });

  //Testing the CustomTextField with errorText
  testWidgets('CustomTextField correctly displays errorText',
      (WidgetTester tester) async {
    // Créez le widget
    await tester.pumpWidget(const MaterialApp(
      home: Scaffold(
        body: CustomTextField(
          hintText: 'Texte 1',
          color: Colors.blue,
          errorColor: Colors.red,
        ),
      ),
    ));

    // Vérifiez que le texte d'indice est affiché correctement
    expect(find.text('Texte 1'), findsOneWidget);
  });
}
