import 'package:a_rosa_je/widgets/widgets.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:flutter/material.dart';

void main() {
  testWidgets('CustomButton calls onPressed when tapped',
      (WidgetTester tester) async {
    var pressed = false;
    // Build our app and trigger a frame.
    await tester.pumpWidget(MaterialApp(
        home: Scaffold(
            body: CustomButton(
      onPressed: () {
        pressed = true;
      },
      label: 'Test',
    ))));

    // Tap the button.
    await tester.tap(find.byType(CustomButton));
    await tester.pump();

    // Verify that the onPressed callback was called.
    expect(pressed, true);
  });
}
