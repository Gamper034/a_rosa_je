import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:a_rosa_je/widgets/button.dart';

void main() {
  const testLabel = 'Test';
  var pressed = false;
  void onPressed() {
    pressed = true;
  }

  testWidgets('CustomButton calls correctly', (WidgetTester tester) async {
    await tester.pumpWidget(
      MaterialApp(
        home: CustomButton(
          onPressed: onPressed,
          label: testLabel,
          icon: Icons.check,
        ),
      ),
    );

    expect(find.text(testLabel), findsOneWidget);

    expect(find.byIcon(Icons.check), findsOneWidget);
  });

  testWidgets('CustomButton onPressed works correctly',
      (WidgetTester tester) async {
    await tester.pumpWidget(
      MaterialApp(
        home: CustomButton(
          onPressed: onPressed,
          label: testLabel,
        ),
      ),
    );

    await tester.tap(find.byType(CustomButton));
    await tester.pump();

    // Vérifie que la fonction onPressed a été appelée
    expect(pressed, isTrue);
  });

  testWidgets('CustomButton renders green color without border',
      (WidgetTester tester) async {
    // Crée le widget
    await tester.pumpWidget(
      MaterialApp(
        home: CustomButton(
          onPressed: onPressed,
          label: testLabel,
          buttonColor: Colors.green,
          border: false,
        ),
      ),
    );

    // Trouve le widget Material
    final material = tester.widget<Material>(find.descendant(
      of: find.byType(CustomButton),
      matching: find.byType(Material),
    ));

    // Vérifie la couleur
    expect(material.color, Colors.green);

    // Vérifie que la bordure n'est pas présente
    expect((material.shape as RoundedRectangleBorder).side.width, 0.0);
  });

  testWidgets('CustomButton renders white color with border',
      (WidgetTester tester) async {
    // Crée le widget
    await tester.pumpWidget(
      MaterialApp(
        home: CustomButton(
          onPressed: onPressed,
          label: testLabel,
          buttonColor: Colors.white,
          border: true,
        ),
      ),
    );

    // Trouve le widget Material
    final material = tester.widget<Material>(find.descendant(
      of: find.byType(CustomButton),
      matching: find.byType(Material),
    ));

    // Vérifie la couleur
    expect(material.color, Colors.white);

    // Vérifie que la bordure est présente
    expect((material.shape as RoundedRectangleBorder).side.width, 2.0);
  });
}
