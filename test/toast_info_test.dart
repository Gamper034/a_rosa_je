// Importez les packages nécessaires
import 'package:flutter_test/flutter_test.dart';
import 'package:flutter/material.dart';
import 'package:a_rosa_je/widgets/toast_info.dart';

void main() {
  // Test pour le titre
  testWidgets('ToastInfo display title correctly', (WidgetTester tester) async {
    await tester.pumpWidget(MaterialApp(
      home: Builder(
        builder: (context) => Scaffold(
          body: ElevatedButton(
            onPressed: () {
              showDialog(
                context: context,
                builder: (context) => ToastInfo(
                  title: 'Titre de test',
                  content: 'Contenu de test',
                  icon: Icons.info,
                  height: 200.0,
                  onPressedConfirm: () {},
                ),
              );
            },
            child: const Text('Afficher le dialogue'),
          ),
        ),
      ),
    ));

    await tester.tap(find.text('Afficher le dialogue'));
    await tester.pumpAndSettle();

    expect(find.text('Titre de test'), findsOneWidget);
  });

  // Test pour le contenu
  testWidgets('ToastInfo display content correclty',
      (WidgetTester tester) async {
    await tester.pumpWidget(MaterialApp(
      home: Builder(
        builder: (context) => Scaffold(
          body: ElevatedButton(
            onPressed: () {
              showDialog(
                context: context,
                builder: (context) => ToastInfo(
                  title: 'Titre de test',
                  content: 'Contenu de test',
                  icon: Icons.info,
                  height: 200.0,
                  onPressedConfirm: () {},
                ),
              );
            },
            child: const Text('Afficher le dialogue'),
          ),
        ),
      ),
    ));

    await tester.tap(find.text('Afficher le dialogue'));
    await tester.pumpAndSettle();

    expect(find.text('Contenu de test'), findsOneWidget);
  });

  // Test pour le bouton
  testWidgets('ToastInfo display button correctly',
      (WidgetTester tester) async {
    await tester.pumpWidget(MaterialApp(
      home: Builder(
        builder: (context) => Scaffold(
          body: ElevatedButton(
            onPressed: () {
              showDialog(
                context: context,
                builder: (context) => ToastInfo(
                  title: 'Titre de test',
                  content: 'Contenu de test',
                  icon: Icons.info,
                  height: 200.0,
                  onPressedConfirm: () {},
                ),
              );
            },
            child: const Text('Afficher le dialogue'),
          ),
        ),
      ),
    ));

    await tester.tap(find.text('Afficher le dialogue'));
    await tester.pumpAndSettle();

    expect(find.text('Fermer'), findsOneWidget);
  });
}
