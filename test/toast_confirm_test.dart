// Importez les packages nécessaires
import 'package:flutter_test/flutter_test.dart';
import 'package:flutter/material.dart';
import 'package:a_rosa_je/widgets/toast_confirm.dart';

void main() {
  // Test pour le titre
  testWidgets('ToastConfirm display title correctly',
      (WidgetTester tester) async {
    await tester.pumpWidget(MaterialApp(
      home: Builder(
        builder: (context) => Scaffold(
          body: ElevatedButton(
            onPressed: () {
              showDialog(
                context: context,
                builder: (context) => ToastConfirm(
                  theme: 'light',
                  title: 'Titre de test',
                  content: 'Contenu de test',
                  icon: Icons.check,
                  height: 200.0,
                  onPressedCancel: () {},
                  onPressedConfirm: () {},
                ),
              );
            },
            child: const Text('Afficher le dialogue'),
          ),
        ),
      ),
    ));

    await tester.tap(find.text('Afficher le dialogue'));
    await tester.pumpAndSettle();

    expect(find.text('Titre de test'), findsOneWidget);
  });

  // Test pour le contenu
  testWidgets('ToastConfirm display content correclty',
      (WidgetTester tester) async {
    await tester.pumpWidget(MaterialApp(
      home: Builder(
        builder: (context) => Scaffold(
          body: ElevatedButton(
            onPressed: () {
              showDialog(
                context: context,
                builder: (context) => ToastConfirm(
                  theme: 'light',
                  title: 'Titre de test',
                  content: 'Contenu de test',
                  icon: Icons.check,
                  height: 200.0,
                  onPressedCancel: () {},
                  onPressedConfirm: () {},
                ),
              );
            },
            child: const Text('Afficher le dialogue'),
          ),
        ),
      ),
    ));

    await tester.tap(find.text('Afficher le dialogue'));
    await tester.pumpAndSettle();

    expect(find.text('Contenu de test'), findsOneWidget);
  });

  // Test pour les boutons
  testWidgets('ToastConfirm display buttons correctly',
      (WidgetTester tester) async {
    await tester.pumpWidget(MaterialApp(
      home: Builder(
        builder: (context) => Scaffold(
          body: ElevatedButton(
            onPressed: () {
              showDialog(
                context: context,
                builder: (context) => ToastConfirm(
                  theme: 'light',
                  title: 'Titre de test',
                  content: 'Contenu de test',
                  icon: Icons.check,
                  height: 200.0,
                  onPressedCancel: () {},
                  onPressedConfirm: () {},
                ),
              );
            },
            child: const Text('Afficher le dialogue'),
          ),
        ),
      ),
    ));

    await tester.tap(find.text('Afficher le dialogue'));
    await tester.pumpAndSettle();

    expect(find.text('Annuler'), findsOneWidget);
    expect(find.text('Valider'), findsOneWidget);
  });
}
